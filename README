<!--- -*-markdown-*- -->

# seqmi

seqmi is a tool for calculating
[mutual information](https://en.wikipedia.org/wiki/Mutual_information)
(MI) between amino acid residues in multiple sequence alignments.  In
addition to computing standard MI, it also supports calculating some
MI corrections/adjustments.

## Installation

If you are building this software directly from a clone of the
development repository, see the file `README-hacking` for additional
instructions on building the software.

Otherwise, simply use the configure script and the Makefile to build
and install seqmi:

    $ ./configure
    $ make all
    $ make install

To see configuration options, run `./configure --help`.  In
particular, the `--prefix` option allows you to set the installation
location and the `--as-published` option enables strict interpretation
of previously published methods (see RCW, below).

Unit tests are available via the `make check` command.  Note that
[DejaGnu](http://www.gnu.org/software/dejagnu) is required to run the
tests.

## Usage

For a quick overview of all of the options available, run seqmi with
the `--help` (`-?`) option or view the provided man page.

At a minimum, you just need an alignment file.  Currently, only FASTA
format is supported.  By default, this will calculate MI for all
positions against all other positions.

    $ seqmi alignment.fasta

For more options, see the included documentation.  If you have texinfo
installed, just run `info seqmi`.  You can build PDF or HTML
documentation by changing to the `doc` directory in this archive and
running `make pdf` or `make html`, respectively.
