/* 
 * alignment.c --- 
 * 
 * Copyright (C) 2014, 2015, 2016, 2019 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "alignment.h"


void
destroy_kseq_copy (kseq_t *cpy)
{
  if (!cpy)
    return;
  if (cpy->name.s)
    free (cpy->name.s);
  if (cpy->comment.s)
    free (cpy->comment.s);
  if (cpy->seq.s)
    free (cpy->seq.s);
  if (cpy->qual.s)
    free (cpy->qual.s);
  free (cpy);
}


void
destroy_alignment (alignment *msa)
{
  unsigned int i;
  if (!msa)
    return;
  for (i=0; i<msa->num_seqs; i++)
    destroy_kseq_copy (msa->seqs[i]);
  free (msa->seqs);
  if (msa->weights)
    gsl_vector_free (msa->weights);
  if (msa->neff)
    gsl_vector_free (msa->neff);
  if (msa->joint_neff)
    gsl_matrix_free (msa->joint_neff);
  if (msa->orig_cols)
    free (msa->orig_cols);
}


kseq_t*
copy_kseq (kseq_t *to, kseq_t *from)
{
  to->name.s = (char *)xmalloc ((from->name.l+1) * sizeof (char));
  strncpy (to->name.s, from->name.s, from->name.l * sizeof (char));
  to->name.l = from->name.l;
  to->name.s[to->name.l] = '\0';
  to->name.m = from->name.m;

  to->comment.s = (char *)xmalloc ((from->comment.l+1) * sizeof (char));
  strncpy (to->comment.s, from->comment.s, from->comment.l * sizeof (char));
  to->comment.l = from->comment.l;
  to->comment.s[to->comment.l] = '\0';
  to->comment.m = from->comment.m;

  to->seq.s = (char *)xmalloc ((from->seq.l+1) * sizeof (char));
  strncpy (to->seq.s, from->seq.s, from->seq.l * sizeof (char));
  to->seq.l = from->seq.l;
  to->seq.s[to->seq.l] = '\0';
  to->seq.m = from->seq.m;

  to->qual.s = (char *)xmalloc ((from->qual.l+1) * sizeof (char));
  strncpy (to->qual.s, from->qual.s, from->qual.l * sizeof (char));
  to->qual.l = from->qual.l;
  to->qual.s[to->qual.l] = '\0';
  to->qual.m = from->qual.m;

  to->last_char = from->last_char;
  to->f = from->f;
  return to;
}


unsigned int
find_ends (alignment *msa)
{
  unsigned int i, j, gaps = 0;
  bool *prev_gaps;
  prev_gaps = (bool *)xcalloc (msa->num_seqs, sizeof (bool));
  memset (prev_gaps, true, msa->num_seqs * sizeof (bool));
  msa->good_start = 0;
  msa->good_end = msa->length;
  for (i=0; i<msa->length; i++)
    {
      for (j=0; j<msa->num_seqs; j++)
        {
          if (prev_gaps[j] && alignment_get_aa (*msa, j, i) == GAP)
            gaps++;
          else
            prev_gaps[j] = false;
        }
      if ((double) gaps / (double) msa->num_seqs < 0.1)
        break;
      msa->good_start++;
      gaps = 0;
    }
  memset (prev_gaps, true, msa->num_seqs * sizeof (bool));
  for (i=msa->length-1; i>0; i--)
    {
      for (j=0; j<msa->num_seqs; j++)
        {
          if (prev_gaps[j] && alignment_get_aa (*msa, j, i) == GAP)
            gaps++;
          else
            prev_gaps[j] = false;
        }
      if ((double) gaps / (double) msa->num_seqs < 0.1)
        break;
      msa->good_end--;
      gaps = 0;
    }
  free (prev_gaps);
}


void
calc_seq_weights (alignment *msa)
{
  /* Calculate sequence weights cf Henikoff & Henikoff 1994.
     Calculate this on a reduced alignment, cf Biegert & Soeding 2009,
     such that only the columns with <10% terminal gaps are included.
     That is, columns at the start or end of the alignment with >= 10%
     gaps are not included. */
  unsigned int i, j, num_res = 0;
  gsl_vector *res_counts, *res_weights;
  aminoacid res;
  msa->weights = gsl_vector_alloc (msa->num_seqs);
  gsl_vector_set_zero (msa->weights);
  res_counts = gsl_vector_alloc (NUM_AA);
  gsl_vector_set_zero (res_counts);
  res_weights = gsl_vector_alloc (NUM_AA);
  gsl_vector_set_all (res_weights, 1.0);
  for (i=0; i<msa->length; i++)
    {
      for (j=0; j<msa->num_seqs; j++)
        {
          res = alignment_get_aa (*msa, j, i);
          if (res == ERR)
            error (EXIT_FAILURE, errno, "Unknown residue in position %d: %c",
                   (int) i, alignment_get_char (*msa, j, i));
          if (aa_is_ambiguous (res))
            continue;
          if (gsl_vector_get (res_counts, res) == 0.0)
            num_res++;
          gsl_vector_set (res_counts, res,
                          gsl_vector_get (res_counts, res) + 1.0);
        }
      gsl_vector_scale (res_counts, (const double)num_res);
      gsl_vector_div (res_weights, res_counts);
      for (j=0; j<msa->num_seqs; j++)
        {
          res = alignment_get_aa (*msa, j, i);
          if (res == ERR)
            error (EXIT_FAILURE, errno, "Unknown residue in position %d: %c",
                   (int) i, alignment_get_char (*msa, j, i));
          if (aa_is_ambiguous (res))
            continue;
          gsl_vector_set (msa->weights, j, gsl_vector_get (msa->weights, j) \
                          + gsl_vector_get (res_weights, res));
        }
      gsl_vector_set_zero (res_counts);
      gsl_vector_set_all (res_weights, 1.0);
      num_res = 0;
    }
  /* Normalize: each column should add up to 1, so the normalization
     factor is just 1*len */
  gsl_vector_scale (msa->weights, 1.0/(double)msa->length);
  gsl_vector_free (res_counts);
  gsl_vector_free (res_weights);
}


size_t
get_neff_subalignment (alignment msa, size_t pos, alignment *sub_msa)
{
  size_t i, j=0, k;
  aminoacid res;
  size_t *good_seqs;
  good_seqs = (size_t *) xmalloc (msa.num_seqs * sizeof (size_t));
  for (i=0; i<msa.num_seqs; i++)
    {
      res = alignment_get_aa (msa, i, pos);
      if (res == ERR)
        error (EXIT_FAILURE, errno, "Unknown residue in position %d: %c",
               (int) pos, alignment_get_char (msa, i, pos));
      if (!aa_is_ambiguous (res))
        {
          good_seqs[j++] = i;
        }
    }
  if (j > 0)
    {
      sub_msa->seqs = (kseq_t **) xcalloc (j, sizeof (kseq_t *));
      for (i=0; i<j; i++)
        {
          k = good_seqs[i];
          sub_msa->seqs[i] = (kseq_t *) xmalloc (sizeof (kseq_t));
          copy_kseq (sub_msa->seqs[i], msa.seqs[k]);
        }
      sub_msa->num_seqs = j;
      sub_msa->length = sub_msa->seqs[0]->seq.l;
      sub_msa->weights = NULL;
      sub_msa->neff = NULL;
      sub_msa->joint_neff = NULL;
      sub_msa->orig_cols = NULL;
      find_ends (sub_msa);
    }
  else
    {
      sub_msa->seqs = NULL;
      sub_msa->num_seqs = 0;
      sub_msa->length = 0;
      sub_msa->weights = NULL;
      sub_msa->neff = NULL;
      sub_msa->joint_neff = NULL;
      sub_msa->orig_cols = NULL;
    }
  free (good_seqs);
}


size_t
get_joint_neff_subalignment (alignment msa, size_t pos1, size_t pos2,
                             alignment *sub_msa)
{
  size_t i, j=0, k;
  aminoacid res1, res2;
  size_t *good_seqs;
  good_seqs = (size_t *) xmalloc (msa.num_seqs * sizeof (size_t));
  for (i=0; i<msa.num_seqs; i++)
    {
      res1 = alignment_get_aa (msa, i, pos1);
      if (res1 == ERR)
        error (EXIT_FAILURE, errno, "Get subaligment: Unknown residue in "
               "sequence %zd position %zd",
               i, pos1);
      res2 = alignment_get_aa (msa, i, pos2);
      if (res2 == ERR)
        error (EXIT_FAILURE, errno, "Get subalignment: Unknown residue in "
               "sequence %zd position %zd",
               i, pos2);
      if (!aa_is_ambiguous (res1) && !aa_is_ambiguous (res2))
        {
          good_seqs[j++] = i;
        }
    }
  if (j > 0)
    {
      sub_msa->seqs = (kseq_t **) xcalloc (j, sizeof (kseq_t *));
      for (i=0; i<j; i++)
        {
          k = good_seqs[i];
          sub_msa->seqs[i] = (kseq_t *) xmalloc (sizeof (kseq_t));
          copy_kseq (sub_msa->seqs[i], msa.seqs[k]);
        }
      sub_msa->num_seqs = j;
      sub_msa->length = sub_msa->seqs[0]->seq.l;
      sub_msa->weights = NULL;
      sub_msa->neff = NULL;
      sub_msa->joint_neff = NULL;
      sub_msa->orig_cols = NULL;
      find_ends (sub_msa);
    }
  else
    {
      sub_msa->seqs = NULL;
      sub_msa->num_seqs = 0;
      sub_msa->length = 0;
      sub_msa->weights = NULL;
      sub_msa->neff = NULL;
      sub_msa->joint_neff = NULL;
      sub_msa->orig_cols = NULL;
    }
  free (good_seqs);
}


double
calc_neff (alignment msa, size_t pos)
{
  /* Calculate the effective number of sequences at a column.  This
     works the same as calculating the effective number of sequences
     cf Biegert & Soeding 2009 */
  size_t i, j;
  aminoacid res;
  double p[NUM_AA] = {0.0};
  double neff = 0.0;
  alignment *sub_msa;
  sub_msa = (alignment *) xmalloc (sizeof (alignment));
  get_neff_subalignment (msa, pos, sub_msa);
  if (sub_msa->num_seqs == 0)
    {
      destroy_alignment (sub_msa);
      return (0);
    }
  calc_seq_weights (sub_msa);
  for (i=sub_msa->good_start; i<sub_msa->good_end; i++)
    {
      for (j=0; j<sub_msa->num_seqs; j++)
        {
          res = alignment_get_aa (*sub_msa, j, i);
          if (res == ERR)
            error (EXIT_FAILURE, errno, "Unknown residue in position %d: %c",
                   (int) pos, alignment_get_char (*sub_msa, j, i));
          if (aa_is_ambiguous (res))
            continue;
          p[res] += gsl_vector_get (sub_msa->weights, j);
        }
      for (j=0; j<NUM_AA; j++)
        {
          if (p[j] > 0.0)
            neff -= p[j] * log (p[j]);
          p[j] = 0.0;
        }
    }
  neff /= (double) sub_msa->good_end - (double) sub_msa->good_start;
  destroy_alignment (sub_msa);
  return exp (neff);
}


double
calc_joint_neff (alignment msa, size_t pos1, size_t pos2)
{
  /* Calculate the effective number of sequences at two columns.  This
     works the same as calculating the effective number of sequences
     cf Biegert & Soeding 2009, however the rows of the aligment used
     are constrained by not having gaps in either of the two columns
     (rather than not having gaps in a specific column) */
  size_t i, j;
  aminoacid res;
  double p[NUM_AA] = {0.0};
  double neff = 0.0;
  alignment *sub_msa;
  sub_msa = (alignment *) xmalloc (sizeof (alignment));
  get_joint_neff_subalignment (msa, pos1, pos2, sub_msa);
  if (sub_msa->num_seqs == 0)
    {
      destroy_alignment (sub_msa);
      return (0);
    }
  calc_seq_weights (sub_msa);
  for (i=sub_msa->good_start; i<sub_msa->good_end; i++)
    {
      for (j=0; j<sub_msa->num_seqs; j++)
        {
          res = alignment_get_aa (*sub_msa, j, i);
          if (res == ERR)
            error (EXIT_FAILURE, errno, "Unknown residue in "
                   "sequence %zd position %zd", j, i);
          if (aa_is_ambiguous (res))
            continue;
          p[res] += gsl_vector_get (sub_msa->weights, j);
        }
      for (j=0; j<NUM_AA; j++)
        {
          if (p[j] > 0.0)
            neff -= p[j] * log (p[j]);
          p[j] = 0.0;
        }
    }
  neff /= (double) sub_msa->good_end - (double) sub_msa->good_start;
  destroy_alignment (sub_msa);
  return exp (neff);
}


void
read_alignment (char *align_file, alignment *msa)
{
  FILE *align_handle;
  kseq_t *seq;
  int status;
  size_t num_seqs = 1;
  size_t i = 0;

  if (!strcmp (align_file, "-"))
    align_handle = stdin;
  else
    align_handle = fopen (align_file, "r");
  if (!align_handle)
    {
      error (EXIT_FAILURE, errno, "Could not open alignment file");
    }
  msa->seqs = (kseq_t **)xcalloc (1, sizeof (kseq_t *));
  seq = kseq_init (fileno (align_handle));
  while ((status = kseq_read (seq)) >= 0)
    {
      if (i == num_seqs)
        {
          msa->seqs = (kseq_t **)xrealloc ((void *)msa->seqs, ++num_seqs * sizeof (kseq_t *));
        }
      msa->seqs[i] = (kseq_t *)xmalloc (sizeof (kseq_t));
      copy_kseq (msa->seqs[i], seq);
      i++;
    }
  fclose (align_handle);
  kseq_destroy (seq);
  if (i == 0 || status == -2)
    {
      destroy_alignment (msa);
      if (i == 0)
        error (EXIT_FAILURE, errno, "No sequences in alignment");
      if (status == -2)
        error (EXIT_FAILURE, errno, "Truncated alignment");
    }
  msa->num_seqs = num_seqs;
  msa->length = msa->seqs[0]->seq.l;
  msa->weights = NULL;
  msa->neff = NULL;
  msa->joint_neff = NULL;
  msa->orig_cols = NULL;
}


size_t
pair_to_1d (size_t length, size_t i, size_t j)
{
  /* This function takes a pair of residues and converts them to an
     index for a one-dimensional array.  The tricky part is that we're
     only interested in the non-empty half of a triangular matrix,
     without the diagonal.  That is, for an alignment of length N, the
     1d array will have (N * (N-1))/2 items. */
  return i*length - (i * (i + 1))/2 + j - i - 1;
}


kseq_t*
find_seq (alignment msa, char *reference)
{
  size_t i;
  for (i=0; i<msa.num_seqs; i++)
    {
      /* If a reference sequence name was given, look for it in the
         alignment. */
      if (!strcmp (msa.seqs[i]->name.s, reference))
        {
          return msa.seqs[i];
        }
    }
  return NULL;
}


int
find_seq_num (alignment msa, char *reference)
{
  int i;
  for (i=0; (size_t)i<msa.num_seqs; i++)
    {
      /* If a reference sequence name was given, look for it in the
         alignment. */
      if (!strcmp (msa.seqs[(size_t)i]->name.s, reference))
        {
          return i;
        }
    }
  return -1;
}


bool
has_gaps (alignment msa, size_t pos)
{
  size_t i;
  for (i=0; i<msa.num_seqs; i++)
    {
      if (aa_is_ambiguous (alignment_get_aa (msa, i, pos)))
        {
          return true;
        }
    }
  return false;
}


double
perc_gaps (alignment msa, size_t pos)
{
  size_t i;
  size_t num_gaps = 0;
  for (i=0; i<msa.num_seqs; i++)
    {
      if (aa_is_ambiguous (alignment_get_aa (msa, i, pos)))
        {
          num_gaps++;
        }
    }
  return (double) num_gaps / (double) msa.num_seqs;
}


bool
skip_column (alignment msa, size_t pos, copts opts)
{
  return ((opts.skip_gaps == 0.0 && has_gaps (msa, pos)) ||
          (opts.skip_gaps > 0.0 && opts.skip_gaps < perc_gaps (msa, pos)));
}


void
remove_gappy_columns (const alignment msa, alignment *degapped_msa,
                      copts opts)
{
  size_t i, j, k=0, s=0, good_chunk_start, good_chunk_end, good_chunk_len;
  unsigned int num_good_cols=0, num_skipped_cols;
  size_t *skipped_cols;
  char *p;
  for (j=0; j<msa.length; j++)
    {
      if (!skip_column (msa, j, opts))
        {
          num_good_cols++;
        }
    }
  num_skipped_cols = msa.length - num_good_cols;
  degapped_msa->seqs = (kseq_t **) xcalloc (msa.num_seqs, sizeof (kseq_t *));
  degapped_msa->orig_cols = (size_t *) xcalloc (num_good_cols, sizeof (size_t));
  degapped_msa->length = num_good_cols;
  degapped_msa->num_seqs = msa.num_seqs;
  degapped_msa->weights = NULL;
  degapped_msa->neff = NULL;
  degapped_msa->joint_neff = NULL;
  if (num_skipped_cols > 0)
    skipped_cols = (size_t *) xcalloc (num_skipped_cols, sizeof (size_t));
  for (j=0; j<msa.length; j++)
    {
      if (!skip_column (msa, j, opts))
        degapped_msa->orig_cols[k++] = j;
      else if (num_skipped_cols > 0)
        skipped_cols[s++] = j;
    }
  for (i=0; i<msa.num_seqs; i++)
    {
      degapped_msa->seqs[i] = (kseq_t *) xmalloc (sizeof (kseq_t));
      degapped_msa->seqs[i]->name.s = (char *)xmalloc
        ((msa.seqs[i]->name.l+1) * sizeof (char));
      strncpy (degapped_msa->seqs[i]->name.s, msa.seqs[i]->name.s,
               msa.seqs[i]->name.l * sizeof (char));
      degapped_msa->seqs[i]->name.l = msa.seqs[i]->name.l;
      degapped_msa->seqs[i]->name.s[degapped_msa->seqs[i]->name.l] = '\0';
      degapped_msa->seqs[i]->name.m = msa.seqs[i]->name.m;

      degapped_msa->seqs[i]->comment.s = (char *)xmalloc
        ((msa.seqs[i]->comment.l+1) * sizeof (char));
      strncpy (degapped_msa->seqs[i]->comment.s, msa.seqs[i]->comment.s,
               msa.seqs[i]->comment.l * sizeof (char));
      degapped_msa->seqs[i]->comment.l = msa.seqs[i]->comment.l;
      degapped_msa->seqs[i]->comment.s[degapped_msa->seqs[i]->comment.l] = '\0';
      degapped_msa->seqs[i]->comment.m = msa.seqs[i]->comment.m;

      degapped_msa->seqs[i]->seq.s = (char *)xmalloc
        ((num_good_cols+1) * sizeof (char));
      if (num_skipped_cols > 0)
        {
          good_chunk_start = 0;
          good_chunk_len = 0;
          p = degapped_msa->seqs[i]->seq.s;
          for (s=0; s<num_skipped_cols; s++)
            {
              if (good_chunk_start >= msa.seqs[i]->seq.l)
                error(EXIT_FAILURE, 0, "Error trying to remove gappy columns");
              good_chunk_end = skipped_cols[s];
              if (good_chunk_start == good_chunk_end)
                {
                  good_chunk_start++;
                  continue;
                }
              good_chunk_len = good_chunk_end - good_chunk_start;
              p = stpncpy (p, msa.seqs[i]->seq.s+good_chunk_start,
                           good_chunk_len);
              good_chunk_start = good_chunk_end + 1;
            }
          if (good_chunk_start < msa.length - 1)
            {
              stpncpy (p, msa.seqs[i]->seq.s+good_chunk_start,
                       msa.length - good_chunk_start);
            }
          degapped_msa->seqs[i]->seq.l = num_good_cols;
        }
      else
        {
          strncpy (degapped_msa->seqs[i]->seq.s, msa.seqs[i]->seq.s,
                   msa.seqs[i]->seq.l * sizeof (char));
          degapped_msa->seqs[i]->seq.l = msa.seqs[i]->seq.l;
        }
      degapped_msa->seqs[i]->seq.s[degapped_msa->seqs[i]->seq.l] = '\0';
      degapped_msa->seqs[i]->seq.m = msa.seqs[i]->seq.m;

      degapped_msa->seqs[i]->qual.s = (char *)xmalloc
        ((msa.seqs[i]->qual.l+1) * sizeof (char));
      strncpy (degapped_msa->seqs[i]->qual.s, msa.seqs[i]->qual.s,
               msa.seqs[i]->qual.l * sizeof (char));
      degapped_msa->seqs[i]->qual.l = msa.seqs[i]->qual.l;
      degapped_msa->seqs[i]->qual.s[degapped_msa->seqs[i]->qual.l] = '\0';
      degapped_msa->seqs[i]->qual.m = msa.seqs[i]->qual.m;
      degapped_msa->seqs[i]->last_char = msa.seqs[i]->last_char;
      degapped_msa->seqs[i]->f = msa.seqs[i]->f;
    }
  if (num_skipped_cols > 0)
    free (skipped_cols);
}
