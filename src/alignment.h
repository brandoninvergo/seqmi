/* 
 * alignment.h --- Define a structure to hold a MSA
 * 
 * Copyright (C) 2014, 2015, 2016 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef ALIGNMENT_H
#define ALIGNMENT_H

#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <string.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include "kseq.h"
#include "xmalloc.h"
#include "aminoacid.h"
#include "options.h"

typedef struct alignment alignment;

KSEQ_INIT(int, read)

struct alignment
{
  kseq_t **seqs;
  size_t length;
  size_t num_seqs;
  gsl_vector *weights;
  gsl_vector *neff;
  gsl_matrix *joint_neff;
  gsl_matrix *aa_freqs;
  size_t good_start, good_end;
  size_t *orig_cols;
};

void destroy_alignment (alignment *msa);
kseq_t *copy_kseq (kseq_t *to, kseq_t *from);
void calc_seq_weights (alignment *msa);
double calc_neff (alignment msa, size_t pos);
double calc_joint_neff (alignment msa, size_t pos1, size_t pos2);
void read_alignment (char *align_file, alignment *msa);
size_t pair_to_1d (size_t length, size_t i, size_t j);
kseq_t *find_seq (alignment msa, char *reference);
int find_seq_num (alignment msa, char *reference);
bool has_gaps (alignment msa, size_t pos);
double perc_gaps (alignment msa, size_t pos);
bool skip_column (alignment msa, size_t pos, copts opts);
void remove_gappy_columns (alignment msa, alignment *degapped_msa, copts opts);

#define alignment_get_aa(MSA, N, P) ((N >= (MSA).num_seqs || P >= (MSA).length) ? \
                                     ERR : char2aa[(size_t) (MSA).seqs[N]->seq.s[P]])
#define alignment_get_char(MSA, N, P) ((N >= (MSA).num_seqs || P >= (MSA).length) ? \
                                       '?' : (MSA).seqs[N]->seq.s[P])

#endif
