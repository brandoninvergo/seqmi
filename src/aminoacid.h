/* 
 * aminoacid.h --- Convenience functions for working with amino acids
 * 
 * Copyright (C) 2014, 2015, 2016 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AMINOACID_H
#define AMINOACID_H

#include <stdbool.h>

typedef enum {
  ARG = 0, HIS, LYS, ASP, GLU, SER, THR, ASN, GLN, CYS, GLY, PRO, ALA, VAL, ILE,
  LEU, MET, PHE, TYR, TRP, NUM_AA, GAP, ANY, STOP, ERR
} aminoacid;

static aminoacid char2aa [129] = {
  ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR,
  ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR,
  ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, STOP, ERR, ERR, GAP, ERR, ERR,
  ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR,
  ERR, ALA, ERR, CYS, ASP, GLU, PHE, GLY, HIS, ILE, ERR, LYS, LEU, MET, ASN, ERR,
  PRO, GLN, ARG, SER, THR, ERR, VAL, TRP, ANY, TYR, ERR, ERR, ERR, ERR, ERR, ERR,
  ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR,
  ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR, ERR,
  ERR
};

#define aa_is_gap(A) (A == GAP)
#define aa_is_ambiguous(A) (A == GAP || A == ANY || A == STOP)

#endif
