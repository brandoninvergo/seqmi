/* 
 * constrain.c --- 
 * 
 * Copyright (C) 2014, 2016 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "constrain.h"

#define MAX_ITERATIONS 1e8
#define TOLERANCE 1e-5


double
vec_sum (gsl_vector *vec)
{
  double sum = 0.0;
  size_t i;
  for (i=0; i<vec->size; i++)
    sum += gsl_vector_get (vec, i);
  return sum;
}


void
update_constraints (gsl_matrix *joint_probs, gsl_vector *AB, gsl_vector *fg,
                    size_t N)
{
  /* Here we update the constraints that the joint probabilities sum
     up to the marginal probabilities.  The constraints are in the
     form:

     f_i (A_i, B) = A_i \sum_j B_j q_{ij} => P_i
     g_j (A, B_j) = B_j \sum_i A_i q_{ij} => P'_j

     P => position 1 marginal probabilities

     P' => position 2 marginal probabilities

     A => vector of N transformed Lagrange multipliers, where A_i =
          exp(\alpha_i - 1) for Lagrange multiplier \alpha

     B => vector of N transformed Lagrange multipliers, where B_j =
          exp(\beta_j) for Lagrange multiplier \beta, B_N = 1

     N => alphabet size

     q => unconstrained joint probabilities matrix

   */

  size_t i;
  double cell;
  gsl_vector *A_cpy, *B_cpy;
  gsl_vector_view A_v, B_v, jp_row, jp_col;
  A_cpy = gsl_vector_alloc (N);
  A_v = gsl_vector_subvector (AB, 0, N);
  B_cpy = gsl_vector_alloc (N);
  B_v = gsl_vector_subvector (AB, N, N);
  for (i=0; i<N; i++)
    {
      /* set up temporary vectors */
      jp_col = gsl_matrix_column (joint_probs, i);
      gsl_vector_memcpy (B_cpy, &B_v.vector);
      /* do calculation for f_i(A_i, B) */
      gsl_vector_mul (B_cpy, &jp_col.vector);
      cell = gsl_vector_get (AB, i) * vec_sum (B_cpy);
      gsl_vector_set (fg, i, cell);
      if (i < N-1)
        {
          /* set up temporary vectors */
          gsl_vector_memcpy (A_cpy, &A_v.vector);
          jp_row = gsl_matrix_row (joint_probs, i);
          /* do calculation for g_j(A, B_j) */
          gsl_vector_mul (A_cpy, &jp_row.vector);
          cell = gsl_vector_get (AB, N+i) * vec_sum (A_cpy);
          gsl_vector_set (fg, N+i, cell);
        }
    }
  gsl_vector_free (A_cpy);
  gsl_vector_free (B_cpy);
}


void
calc_w (gsl_matrix *joint_probs, gsl_matrix *W, gsl_vector *AB, size_t N)
{
  size_t m;
  gsl_matrix *BA, *ul_quad_I, *jp_ul_quad_tmp, *jp_ul_quad;
  gsl_matrix *lr_quad_I, *jp_lr_quad_tmp, *jp_lr_quad;
  gsl_matrix *jp_ur_quad_tmp, *jp_ur_quad;
  gsl_matrix *jp_ll_quad_tmp, *jp_ll_quad;
  gsl_matrix *W_ul, *W_ul_tmp, *W_lr, *W_lr_tmp;
  gsl_matrix *W_ur, *W_ll;
  gsl_matrix *I;
  gsl_matrix *ones_row;
  gsl_matrix_view AB_m, W_view;

  BA = gsl_matrix_alloc (2*N, 1);
  memcpy (BA->data, AB->data+N, N*sizeof (double));
  memcpy (BA->data+N, AB->data, N*sizeof (double));
  AB_m = gsl_matrix_view_vector (AB, 2*N, 1);
  I = gsl_matrix_alloc (2*N, 2*N);
  gsl_matrix_set_identity (I);
  ones_row = gsl_matrix_alloc (1, 2*N);
  gsl_matrix_set_all (ones_row, 1.0);
  ul_quad_I = gsl_matrix_alloc (2*N, N);
  gsl_matrix_set_identity (ul_quad_I);
  lr_quad_I = gsl_matrix_alloc (2*N, N);
  gsl_matrix_set_zero (lr_quad_I);
  for (m=0; m<N; m++)
    {
      gsl_matrix_set (lr_quad_I, m+N, m, 1.0);
    }

  /* Make an (2*N) x (2*N) matrix storing joint_probs in the
     upper-left quadrant */
  jp_ul_quad = gsl_matrix_alloc (2*N, 2*N);
  jp_ul_quad_tmp = gsl_matrix_alloc (2*N, N);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, ul_quad_I, joint_probs,
                  0.0, jp_ul_quad_tmp);
  gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, jp_ul_quad_tmp, ul_quad_I,
                  0.0, jp_ul_quad);
  /* Construct the upper-left quadrant of W (2N x 2N for now): matrix
     multiply the new joint-probs matrix by the BA vector, and then
     transform the result to be on the diagonal of a 2N x 2N matrix. */
  W_ul = gsl_matrix_alloc (2*N, 2*N);
  W_ul_tmp = gsl_matrix_alloc (2*N, 1);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, jp_ul_quad, BA, 0.0,
                  W_ul_tmp);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, W_ul_tmp, ones_row, 0.0,
                  W_ul);
  gsl_matrix_mul_elements (W_ul, I);
  gsl_matrix_free (jp_ul_quad_tmp);
  gsl_matrix_free (jp_ul_quad);
  gsl_matrix_free (W_ul_tmp);

  /* (2*N)x(2*N) matrix for storing joint_probs in the lower-right
     quadrant */
  jp_lr_quad = gsl_matrix_alloc (2*N, 2*N);
  jp_lr_quad_tmp = gsl_matrix_alloc (2*N, N);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, lr_quad_I, joint_probs,
                  0.0, jp_lr_quad_tmp);
  gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, jp_lr_quad_tmp, lr_quad_I,
                  0.0, jp_lr_quad);
  /* Construct the lower-right quadrant of W (2N x 2N for now): same
     as for the upper-left quadrant, except using the transpose of the
     joint-probs matrix*/
  W_lr = gsl_matrix_alloc (2*N, 2*N);
  W_lr_tmp = gsl_matrix_alloc (2*N, 1);
  gsl_blas_dgemm (CblasTrans, CblasNoTrans, 1.0, jp_lr_quad, BA, 0.0,
                  W_lr_tmp);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, W_lr_tmp, ones_row, 0.0,
                  W_lr);
  gsl_matrix_mul_elements (W_lr, I);
  gsl_matrix_free (jp_lr_quad_tmp);
  gsl_matrix_free (jp_lr_quad);
  gsl_matrix_free (W_lr_tmp);

  /* upper-right joint-prob */
  jp_ur_quad = gsl_matrix_alloc (2*N, 2*N);
  jp_ur_quad_tmp = gsl_matrix_alloc (2*N, N);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, ul_quad_I, joint_probs,
                  0.0, jp_ur_quad_tmp);
  gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, jp_ur_quad_tmp, lr_quad_I,
                  0.0, jp_ur_quad);
  /* upper-right W: create a 2N x 2N matrix in which every column is
     the AB vector, and element-wise multiply with the joint-probs matrix */
  W_ur = gsl_matrix_alloc (2*N, 2*N);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, &AB_m.matrix, ones_row, 0.0,
                  W_ur);
  gsl_matrix_mul_elements (W_ur, jp_ur_quad);
  gsl_matrix_free (jp_ur_quad_tmp);
  gsl_matrix_free (jp_ur_quad);

  /* lower-left joint-prob */
  jp_ll_quad = gsl_matrix_alloc (2*N, 2*N);
  jp_ll_quad_tmp = gsl_matrix_alloc (2*N, N);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, lr_quad_I, joint_probs,
                  0.0, jp_ll_quad_tmp);
  gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, jp_ll_quad_tmp, ul_quad_I,
                  0.0, jp_ll_quad);
  /* lower-left W: same as for upper-right */
  W_ll = gsl_matrix_alloc (2*N, 2*N);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, &AB_m.matrix, ones_row, 0.0,
                  W_ll);
  gsl_matrix_mul_elements (W_ll, jp_ll_quad);
  gsl_matrix_free (jp_ll_quad_tmp);
  gsl_matrix_free (jp_ll_quad);

  gsl_matrix_free (ul_quad_I);
  gsl_matrix_free (lr_quad_I);
  gsl_matrix_free (I);
  gsl_matrix_free (ones_row);
  gsl_matrix_free (BA);

  /* Combine all the quadrant matrices into one */
  gsl_matrix_add (W_ul, W_ur);
  gsl_matrix_add (W_ul, W_ll);
  gsl_matrix_add (W_ul, W_lr);

  /* Make a view of just 2*N-1 rows and columns and copy it into W */
  W_view = gsl_matrix_submatrix (W_ul, 0, 0, 2*N-1, 2*N-1);
  gsl_matrix_memcpy (W, &W_view.matrix);

  gsl_matrix_free (W_ul);
  gsl_matrix_free (W_ur);
  gsl_matrix_free (W_ll);
  gsl_matrix_free (W_lr);
}


void
calc_dfg (gsl_vector *fg, gsl_vector *dfg, gsl_vector *prf_i, gsl_vector *prf_j,
          size_t N)
{
  size_t i;
  for (i=0; i<N; i++)
    {
      gsl_vector_set (dfg, i, gsl_vector_get (prf_i, i) -
                      gsl_vector_get (fg, i));
      if (i < N-1)
        gsl_vector_set (dfg, N+i, gsl_vector_get (prf_j, i) -
                        gsl_vector_get (fg, N+i));
    }

}


double
estimate_error (gsl_vector *dAB)
{
  return gsl_blas_dnrm2 (dAB);
}


void
constrain_joint_probs (gsl_matrix *joint_probs, size_t pos1, size_t pos2,
                       cparams params)
{
  gsl_vector *AB, *fg, *dAB, *dfg;
  gsl_matrix *W;
  gsl_vector_view prf_row1, prf_row2, ABv;
  gsl_permutation *perm;
  int signum = 0;
  size_t N = params.num_vars;
  size_t i = 0, j;
  AB = gsl_vector_alloc (2*N);
  /* Initial guess */
  gsl_vector_set_all (AB, 1.0);
  /* Last element is 0 */
  gsl_vector_set (AB, 2*N-1, 0.0);
  ABv = gsl_vector_subvector (AB, 0, 2*N-1);
  fg = gsl_vector_alloc (2*N-1);
  dAB = gsl_vector_alloc (2*N-1);
  gsl_vector_set_zero (dAB);
  dfg = gsl_vector_alloc (2*N-1);
  W = gsl_matrix_alloc (2*N-1, 2*N-1);
  perm = gsl_permutation_alloc (2*N-1);
  prf_row1 = gsl_matrix_row (params.prf->p, pos1);
  prf_row2 = gsl_matrix_row (params.prf->p, pos2);
  do
    {
      update_constraints (joint_probs, AB, fg, N);
      calc_dfg (fg, dfg, &prf_row1.vector, &prf_row2.vector, N);
      calc_w (joint_probs, W, AB, N);
      gsl_linalg_LU_decomp (W, perm, &signum);
      gsl_linalg_LU_solve (W, perm, dfg, dAB);
      gsl_vector_add (&ABv.vector, dAB);
      if (++i >= MAX_ITERATIONS)
        error (EXIT_FAILURE, errno,
               "Profile-based joint probability constraint: max iterations "
               "reached");
    }
  while (estimate_error (dAB) > TOLERANCE);
  for (i=0; i<N; i++)
    {
      for (j=0; j<N; j++)
        {
          if (j < N-1)
            gsl_matrix_set (joint_probs, i, j,
                            gsl_vector_get (AB, i) * gsl_vector_get (AB, N+j) *
                            gsl_matrix_get (joint_probs, i, j));
          else
            gsl_matrix_set (joint_probs, i, j,
                            gsl_vector_get (AB, i) *
                            gsl_matrix_get (joint_probs, i, j));
        }
    }
  gsl_vector_free (AB);
  gsl_vector_free (fg);
  gsl_vector_free (dAB);
  gsl_vector_free (dfg);
  gsl_matrix_free (W);
  gsl_permutation_free (perm);
}

