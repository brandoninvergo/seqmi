/* 
 * entropy.c --- Calculate information entropy
 * 
 * Copyright (C) 2014, 2015, 2016 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "entropy.h"


double
entropy (gsl_vector *marg_probs)
{
  size_t i;
  double h = 0.0;
  double p;
  for (i=0; i<marg_probs->size; i++)
    {
      p = gsl_vector_get (marg_probs, i);
      if (p > 0.0)
        h += p * log (p) / log (20);
    }
  return -h;
}


double
joint_entropy (gsl_matrix *joint_probs)
{
  size_t i;
  double h = 0.0;
  double p;
  for (i=0; i<joint_probs->size1*joint_probs->size2; i++)
    {
      p = joint_probs->data[i];
      if (p > 0.0)
        h += p * log(p) / log (20);
    }
  return -h;
}


void
calc_entropy_allvsall (gsl_matrix *joint_entropies, gsl_matrix *col1_entropies,
                       gsl_matrix *col2_entropies, alignment msa, copts opts,
                       cparams params)
{
  size_t i, j, nseqs;
  double col_h, joint_h;
  gsl_matrix *counts;
  gsl_matrix *joint_probs;
  gsl_vector *pos1_marg_probs, *pos2_marg_probs;
  counts = gsl_matrix_alloc (params.num_vars, params.num_vars);
  joint_probs = gsl_matrix_alloc (params.num_vars, params.num_vars);
  pos1_marg_probs = gsl_vector_calloc (params.num_vars);
  pos2_marg_probs = gsl_vector_calloc (params.num_vars);
  for (i=0; i<msa.length; i++)
    {
      for (j=i; j<msa.length; j++)
        {
          /* Fill in contingency table: */
          nseqs = contingency_table (msa, i, j, counts, opts);
          /* Calculate joint probabilities. */
          if (params.joint_probs && params.joint_probs[pair_to_1d (msa.length, i, j)])
            gsl_matrix_memcpy (joint_probs,
                               params.joint_probs[pair_to_1d (msa.length, i, j)]);
          else
            calc_joint_probs (counts, msa, nseqs, i, j, joint_probs, opts, params);
          /* Calculate marginal probabilities. */
          calc_marg_probs (joint_probs, pos1_marg_probs, pos2_marg_probs, i, j,
                           opts, params);
          col_h = entropy (pos1_marg_probs);
          gsl_matrix_set (col1_entropies, i, j, col_h);
          gsl_matrix_set (col1_entropies, j, i, col_h);
          col_h = entropy (pos2_marg_probs);
          gsl_matrix_set (col2_entropies, i, j, col_h);
          gsl_matrix_set (col2_entropies, j, i, col_h);
          joint_h = joint_entropy (joint_probs);
          gsl_matrix_set (joint_entropies, i, j, joint_h);
          gsl_matrix_set (joint_entropies, j, i, joint_h);
        }
    }
  gsl_matrix_free (counts);
  gsl_matrix_free (joint_probs);
  gsl_vector_free (pos1_marg_probs);
  gsl_vector_free (pos2_marg_probs);
}
