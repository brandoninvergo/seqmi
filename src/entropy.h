/* 
 * entropy.h --- Calculate information entropy
 * 
 * Copyright (C) 2014, 2015 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ENTROPY_H
#define ENTROPY_H

#include "error.h"
#include <errno.h>
#include <math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include "kseq.h"
#include "xmalloc.h"
#include "alignment.h"
#include "aminoacid.h"
#include "probs.h"
#include "profile.h"
#include "options.h"

struct col_entropies
{
  double h1;
  double h2;
  double joint_h;
};

double entropy (gsl_vector *marg_probs);
double joint_entropy (gsl_matrix *joint_probs);
void calc_entropy_allvsall (gsl_matrix *joint_entropies, gsl_matrix *col1_entropies,
                            gsl_matrix *col2_entropies, alignment msa, copts opts,
                            cparams params);

#endif
