/* 
 * mi.c --- Calculate the mutual information between two sites in an
 * alignment
 * 
 * Copyright (C) 2014, 2015, 2016 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "mi.h"


double
calc_mi (double h1, double h2, double joint_h)
{
  /* Calculate the mutual information over amino acid frequencies at
     two sites. */
  return h1 + h2 - joint_h;
}


void
calc_mi_allvsall (gsl_matrix  *joint_entropies, gsl_matrix *col1_entropies,
                  gsl_matrix *col2_entropies, gsl_matrix *mi_allvsall,
                  size_t align_len)
{
  size_t i, j;
  double mi_ij, h_i, h_j, h_ij;
  gsl_matrix *mi_trans;
  gsl_matrix_set_zero (mi_allvsall);
  for (i=0; i<align_len; i++)
    {
      for (j=i; j<align_len; j++)
        {
          h_i = gsl_matrix_get (col1_entropies, i, j);
          h_j = gsl_matrix_get (col2_entropies, i, j);
          h_ij = gsl_matrix_get (joint_entropies, i, j);
          mi_ij = calc_mi (h_i, h_j, h_ij);
          gsl_matrix_set (mi_allvsall, i, j, mi_ij);
        }
    }
  mi_trans = gsl_matrix_alloc (align_len, align_len);
  gsl_matrix_transpose_memcpy (mi_trans, mi_allvsall);
  /* The above calculations are upper-triangular and include the
     diagonal, so remove the diagonal from the transpose */
  for (i=0; i<align_len; i++)
    gsl_matrix_set (mi_trans, i, i, 0.0);
  gsl_matrix_add (mi_allvsall, mi_trans);
  gsl_matrix_free (mi_trans);
}


double
calc_avg_mi (gsl_vector *pos_avg_mi)
{
  return gsl_stats_mean (pos_avg_mi->data, pos_avg_mi->stride, pos_avg_mi->size);
}


void
calc_pos_avg_mi (gsl_matrix *mi_allvsall, size_t align_len,
                 gsl_vector *pos_avg_mi)
{
  gsl_vector *unity_v;
  gsl_vector_view diag;

  unity_v = gsl_vector_alloc (align_len);
  gsl_vector_set_all (unity_v, 1.0);
  diag = gsl_matrix_diagonal (mi_allvsall);

  /* MI pos sums: A*[1] */
  gsl_blas_dgemv (CblasNoTrans, 1.0, mi_allvsall, unity_v, 0.0, pos_avg_mi);
  gsl_vector_sub (pos_avg_mi, &diag.vector);
  gsl_vector_scale (pos_avg_mi, 1.0 / ((double) align_len-1.0));

  gsl_vector_free (unity_v);
}


void
calc_rcw (gsl_matrix *mi_allvsall, gsl_matrix *adj_mi, bool as_published)
{
  /* RCW(a, b) = MIa,b / ((sum(MIa,.) + sum(MI.,b) - 2*MIa,b)/(2n-2))
     Note: in the original paper, they refer to summing over rows and
     columns on the upper-triangular MI matrix.  Of course, this is
     the same as summing over only the columns (or rows) in the full
     matrix, which is easier and what is implemented below.

     Note 2: I believe that the original formulation is incorrect.
     The weighting consists of the column sums, omitting the MI of the
     two positions in question from each sum, and dividing by twice
     the number of columns minus two.  Effectively, this is the
     average MI in each column, not counting the MI for the pair of
     positions in question.  However, this means that they are
     including the MI of each position with itself which shouldn't be
     considered.  Therefore, a "fixed" version is provided in which
     the column average is calculated by dividing by (2n-4), that is,
     twice the number of columns minus 2 for the two MIa,b values
     removed and minus a further 2 for the MIa,a and MIb,b that are
     being ignored.  The end result is just scaling by a different
     constant, so it shouldn't perform differently than the original
     formulation, but it is more correct.
 */
  gsl_matrix *col_sums, *unity;
  gsl_matrix *weight, *tmp;
  gsl_matrix *I, *diag_m, *diag;

  col_sums = gsl_matrix_alloc (mi_allvsall->size1, 1);
  weight = gsl_matrix_alloc (mi_allvsall->size1, mi_allvsall->size2);
  tmp = gsl_matrix_alloc (mi_allvsall->size1, mi_allvsall->size2);
  unity = gsl_matrix_alloc (mi_allvsall->size1, 1);
  gsl_matrix_set_all (unity, 1.0);
  /* Get the column sums for all columns in the all-vs-all MI matrix
     by multiplying by a nx1 of 1.0 values (keeping everything in
     matrices instead of vectors in order to facilitate later
     operations) */
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, mi_allvsall, unity, 0.0,
                  col_sums);
  /* Multiply the nx1 matrix of column sums by a 1xn matrix of 1.0
     values.  This creates an nxn matrix where each column has the
     column sums */
  gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, col_sums, unity, 0.0, weight);
  gsl_matrix_free (col_sums);
  /* Transpose this matrix and add it to itself.  This gives the
     sum of the two column weights for each pair of positions. */
  gsl_matrix_transpose_memcpy (tmp, weight);
  gsl_matrix_add (weight, tmp);
  /* Now subtract the two times the MI value for each position */
  gsl_matrix_memcpy (tmp, mi_allvsall);
  gsl_matrix_scale (tmp, 2.0);
  gsl_matrix_sub (weight, tmp);
  if (as_published)
    {
      /* As described in Gouveia-Oliveira and Pedersen.  Remove MI_ij from
         the average but do not remove MI_ii, MI_jj.  This is incorrect,
         since you should not consider the mutual information between a
         site and itself.  Furthermore, from their description, it appears
         that they would count MI_ii/MI_jj twice: */
      /* Divide by 2n-2, where n is the number of positions
         to arrive at the final weighting */
      gsl_matrix_scale (weight, 1.0/(2.0*(double) mi_allvsall->size1 - 2.0));
    }
  else
    {
      /* Remove the diagonal */
      I = gsl_matrix_alloc (mi_allvsall->size1, mi_allvsall->size2);
      diag_m = gsl_matrix_alloc (mi_allvsall->size1, mi_allvsall->size2);
      diag = gsl_matrix_alloc (mi_allvsall->size1, 1);

      gsl_matrix_set_identity (I);
      gsl_matrix_memcpy (diag_m, mi_allvsall);
      gsl_matrix_mul_elements (diag_m, I);
      gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, diag_m, unity, 0.0,
                      diag);
      /* Transform the diagonal vector into an nxn matrix, each column
         containing the diagonal vector */
      gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, diag, unity, 0.0, diag_m);
      /* Transpose it and add.  This gives an nxn matrix in which each
         cell contains the sum of the diagonal values for that row and column */
      gsl_matrix_transpose_memcpy (tmp, diag_m);
      gsl_matrix_add (diag_m, tmp);
      /* Remove these diagonal values from the weights */
      gsl_matrix_sub (weight, diag_m);

      gsl_matrix_free (I);
      gsl_matrix_free (diag);
      gsl_matrix_free (diag_m);

      /* Divide by 2n-4.  In this case, we're not only removing MIi,j
         from the scaling, but also MIi,i and MIj,j, since they
         don't contribute anymore to the column sums */
      gsl_matrix_scale (weight, 1.0/(2.0*(double) mi_allvsall->size1 - 4.0));
    }
  gsl_matrix_free (unity);
  gsl_matrix_free (tmp);
  /* Finally, divide element-wise the MI values by this weighting matrix */
  gsl_matrix_memcpy (adj_mi, mi_allvsall);
  gsl_matrix_div_elements (adj_mi, weight);
  gsl_matrix_free (weight);
}


void
calc_apc (gsl_matrix *mi_allvsall, gsl_matrix *adj_mi, gsl_vector *pos_avg_mi,
          double mi_avg)
{
  /* APC(a,b) =  (avg(MIa) * avg(MIb)) / avg(MI)
     for positions a and b.

    Corrected MIa,b = MIa,b - ASC(a,b)
  */

  gsl_matrix_view avg_mi_mat;
  gsl_matrix *apc;

  if (mi_avg == 0.0)
    {
      gsl_matrix_set_all (adj_mi, GSL_NAN);
      return;
    }
  /* Create a matrix version of the vector of position MI averages */
  avg_mi_mat = gsl_matrix_view_vector (pos_avg_mi, pos_avg_mi->size, 1);
  apc = gsl_matrix_alloc (pos_avg_mi->size, pos_avg_mi->size);
  /* Multiply this matrix (nx1) by it's transpose (1xn) to give an nxn
     matrix of position-specific average MI products for each pair of
     positions */
  gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, &avg_mi_mat.matrix,
                  &avg_mi_mat.matrix, 0.0, apc);
  /* Divide by the overall average MI */
  gsl_matrix_scale (apc, 1.0/mi_avg);
  gsl_matrix_memcpy (adj_mi, mi_allvsall);
  /* Subtract element-wise this matrix from the all-vs-all MI matrix
     to give the adjusted MI value for each pair of positions */
  gsl_matrix_sub (adj_mi, apc);
  gsl_matrix_free (apc);
}


void
calc_asc (gsl_matrix *mi_allvsall, gsl_matrix *adj_mi, gsl_vector *pos_avg_mi,
          double mi_avg)
{
  /* ASC(a,b) =  avg(MIa) + avg(MIb) - avg(MI)
     for positions a and b.

    Corrected MIa,b = MIa,b - ASC(a,b)
  */

  gsl_matrix_view avg_mi_mat;
  gsl_matrix *asc, *tmp, *unity;

  /* Create a matrix version of the vector of position MI averages  */
  avg_mi_mat = gsl_matrix_view_vector (pos_avg_mi, pos_avg_mi->size, 1);
  asc = gsl_matrix_alloc (pos_avg_mi->size, pos_avg_mi->size);
  tmp = gsl_matrix_alloc (pos_avg_mi->size, pos_avg_mi->size);
  unity = gsl_matrix_alloc (1, pos_avg_mi->size);
  gsl_matrix_set_all (unity, 1.0);
  /* Multiply the nx1 matrix of MI averages by a 1xn matrix of 1.0
     values.  This creates an nxn matrix where each column has the
     vector of position-specific MI averages */
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, &avg_mi_mat.matrix,
                  unity, 0.0, asc);
  /* Transpose this matrix and add it to itself.  This gives the sum
     of position-specific averages for each pair of positions. */
  gsl_matrix_transpose_memcpy (tmp, asc);
  gsl_matrix_add (asc, tmp);
  /* Subtract the overall average MI from all values */
  gsl_matrix_add_constant (asc, -mi_avg);
  /* Subtract element-wise this matrix from the all-vs-all MI matrix
     to give the adjusted MI value for each pair of positions */
  gsl_matrix_memcpy (adj_mi, mi_allvsall);
  gsl_matrix_sub (adj_mi, asc);
  gsl_matrix_free (asc);
  gsl_matrix_free (tmp);
  gsl_matrix_free (unity);
}


void
calc_jen (gsl_matrix *mi_allvsall, gsl_matrix *adj_mi, gsl_matrix *joint_entropies)
{
  gsl_matrix_memcpy (adj_mi, mi_allvsall);
  gsl_matrix_div_elements (adj_mi, joint_entropies);
}


void
calc_amen (gsl_matrix *mi_allvsall, gsl_matrix *adj_mi,
           gsl_matrix *col1_entropies, gsl_matrix *col2_entropies)
{
  gsl_matrix *tmp;
  tmp = gsl_matrix_alloc (col1_entropies->size1, col1_entropies->size2);
  gsl_matrix_memcpy (tmp, col1_entropies);
  gsl_matrix_memcpy (adj_mi, mi_allvsall);
  gsl_matrix_add (tmp, col2_entropies);
  gsl_matrix_scale (tmp, 0.5);
  gsl_matrix_div_elements (adj_mi, tmp);
}


void
calc_mmen (gsl_matrix *mi_allvsall, gsl_matrix *adj_mi,
          gsl_matrix *col1_entropies, gsl_matrix *col2_entropies)
{
  size_t i;
  gsl_matrix *tmp;
  tmp = gsl_matrix_alloc (col1_entropies->size1, col1_entropies->size2);
  gsl_matrix_memcpy (tmp, col1_entropies);
  gsl_matrix_memcpy (adj_mi, mi_allvsall);
  gsl_matrix_mul_elements (tmp, col2_entropies);
  for (i=0; i<tmp->size1*tmp->size2; i++)
    {
      tmp->data[i] = sqrt(tmp->data[i]);
    }
  gsl_matrix_div_elements (adj_mi, tmp);
}


double
calc_ms (alignment msa, size_t pos, bool adjust, copts opts,
         cparams params)
{
  /* Calculate multiple significant interdependency, cf Tillier and Lui
     2003 Bioinformatics.  If parameter 'adjust' is true, adjust the
     m = msa.num_seqs;
     calculation for missing values due to gaps. */
  size_t i, j, p, q, m, nseqs, num_gaps1 = 0, num_gaps2 = 0;
  double smi, ms = 0.0, x, y, z, lev, resid, sigma;
  gsl_matrix *counts, *expect, *resids;
  gsl_matrix *joint_probs;
  gsl_matrix *design_mat, *design_v;
  gsl_vector *pos1_marg_probs, *pos2_marg_probs;
  gsl_vector *work, *design_s, *leverage, *tmp;
  gsl_matrix_view p1_marg_probs_m, p2_marg_probs_m;
  gsl_vector_view resids_v, expect_v;
  m = msa.num_seqs;
  counts = gsl_matrix_alloc (params.num_vars, params.num_vars);
  joint_probs = gsl_matrix_alloc (params.num_vars, params.num_vars);
  expect = gsl_matrix_alloc (params.num_vars, params.num_vars);
  pos1_marg_probs = gsl_vector_alloc (params.num_vars);
  pos2_marg_probs = gsl_vector_alloc (params.num_vars);
  design_mat = gsl_matrix_alloc (params.num_vars * params.num_vars, 2);
  gsl_matrix_set_all (design_mat, 1.0);
  work = gsl_vector_alloc (2);
  design_v = gsl_matrix_alloc (2, 2);
  design_s = gsl_vector_alloc (2);
  leverage = gsl_vector_alloc (params.num_vars * params.num_vars);
  resids = gsl_matrix_alloc (params.num_vars, params.num_vars);
  tmp = gsl_vector_alloc (params.num_vars * params.num_vars);
  if (adjust)
    {
      for (j=0; j<m; j++)
        {
          if (alignment_get_aa (msa, j, pos) == GAP)
            num_gaps1++;
        }
      if (num_gaps1 == m)
        return 0.0;
    }
  for (i=0; i<msa.length; i++)
    {
      if (i == pos)
        continue;
      /* We need the joint probabilities and marginal probabilities
         for our position and the current column. */
      nseqs = contingency_table (msa, pos, i, counts, opts);
      if (params.joint_probs && params.joint_probs[pair_to_1d (msa.length, pos, i)])
        gsl_matrix_memcpy (joint_probs,
                           params.joint_probs[pair_to_1d (msa.length, pos, i)]);
      else
        {
          calc_joint_probs (counts, msa, nseqs, pos, i, joint_probs, opts, params);
        }
      calc_marg_probs (joint_probs, pos1_marg_probs, pos2_marg_probs, pos, i,
                       opts, params);
      /* The self-mutual information (actually, a running sum of all
         calculated I(e_ij) */
      smi = 0.0;
      num_gaps2 = 0;
      /* Find the number of gaps in this position  */
      if (adjust)
        {
          for (j=0; j<m; j++)
            {
              if (alignment_get_aa (msa, j, i) == GAP)
                num_gaps2++;
            }
          if (num_gaps2 == m)
            continue;
        }
      /* Calculate the standardized residuals from the model obs(e_ij)
         = a0 + a1*exp(e_ij) (obs=observed frequency, exp=expected
         frequency)*/
      p1_marg_probs_m = gsl_matrix_view_vector (pos1_marg_probs,
                                                params.num_vars, 1);
      p2_marg_probs_m = gsl_matrix_view_vector (pos2_marg_probs,
                                                1, params.num_vars);
      gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, &p1_marg_probs_m.matrix,
                      &p2_marg_probs_m.matrix, 0.0, expect);
      gsl_matrix_scale (expect, (double) msa.num_seqs);
      /* In order to studentize the residuals, which will be tested
         for significance, we need the leverage of each point.  The
         model is obs(e_ij) = a0 + a1*exp(e_ij). Fill a model design
         matrix with each data point (exp(e_ij)) and then calculate
         the leverage on this matrix. */
      gsl_matrix_set_all (design_mat, 1.0);
      expect_v = gsl_vector_view_array (expect->data,
                                        params.num_vars*params.num_vars);
      gsl_matrix_set_col (design_mat, 1, &expect_v.vector);
      /* Now calculate the residuals themselves */
      gsl_matrix_memcpy (resids, counts);
      gsl_matrix_sub (resids, expect);
      for (p=0; p<params.num_vars*params.num_vars; p++)
        {
          expect->data[p] = sqrt (expect->data[p]);
        }
      gsl_matrix_div_elements (resids, expect);
      resids_v = gsl_vector_view_array (resids->data, params.num_vars*params.num_vars);
      /* Find \sigma^2 for internal studentization */
      gsl_vector_memcpy (tmp, &resids_v.vector);
      gsl_vector_mul (tmp, tmp);
      /* sigma = gsl_blas_dasum (tmp); */
      sigma = 0.0;
      for (p=0; p<params.num_vars*params.num_vars; p++)
        {
          x = gsl_vector_get (tmp, p);
          if (!gsl_isnan (x) && !gsl_isinf (x))
            sigma += x;
        }
      /* times 1/(n - m), where m is the number of parameters in the
         model (2).  This results in sigma^2, so sqrt. */
      sigma = sqrt (sigma/(params.num_vars*params.num_vars-2));
      gsl_linalg_SV_decomp (design_mat, design_v, design_s, work);
      gsl_linalg_SV_leverage (design_mat, leverage);
      /* finally, adjust the residuals by 1/(sigma * sqrt(1-leverage)) */
      for (p=0; p<params.num_vars*params.num_vars; p++)
        {
          lev = gsl_vector_get (leverage, p);
          gsl_vector_set (leverage, p, sqrt (1 - lev));
        }
      gsl_vector_scale (leverage, sigma);
      gsl_vector_div (&resids_v.vector, leverage);
      /* For each residual, if it's significant, calculate self mutual
         information and add it to the running total */
      for (p=0; p<params.num_vars; p++)
        {
          x = gsl_vector_get (pos1_marg_probs, p);
          for (q=0; q<params.num_vars; q++)
            {
              y = gsl_vector_get (pos2_marg_probs, q);
              z = gsl_matrix_get (joint_probs, p, q);
              resid = gsl_vector_get (&resids_v.vector, p*params.num_vars + q);
              if (resid > 2.326348)
                smi += log (z/(x * y)) / log (20);
            }
        }
      if (adjust)
        {
          ms += smi/((((double)m-num_gaps1)/m)*(((double)m-num_gaps2)/m));
        }
      else
        ms += smi;
    }
  gsl_matrix_free (counts);
  gsl_matrix_free (joint_probs);
  gsl_vector_free (pos1_marg_probs);
  gsl_vector_free (pos2_marg_probs);
  gsl_matrix_free (design_mat);
  gsl_vector_free (work);
  gsl_matrix_free (design_v);
  gsl_vector_free (design_s);
  gsl_vector_free (leverage);
  gsl_matrix_free (resids);
  gsl_matrix_free (expect);
  gsl_vector_free (tmp);
  return ms;
}


double
calc_dep_ratio (gsl_matrix *mi_allvsall, gsl_matrix *dep_ratio, alignment msa,
                bool adjust, copts opts, cparams params)
{
  /* Calculate the dependency ratio, c.f. Tillier and Lui 2003
     Bioinformatics
     = MI / (MSIi + MSIj)
  */
  size_t i;
  gsl_matrix *ms, *tmp, *unity, *dep;

  ms = gsl_matrix_alloc (msa.length, 1);
  tmp = gsl_matrix_alloc (msa.length, msa.length);
  dep = gsl_matrix_alloc (msa.length, msa.length);
  unity = gsl_matrix_alloc (1, msa.length);
  gsl_matrix_set_all (unity, 1.0);
  for (i=0; i<msa.length; i++)
    {
      gsl_matrix_set (ms, i, 0, calc_ms (msa, i, adjust, opts, params));
    }
  /* Multiply the nx1 matrix of MS averages by a 1xn matrix of 1.0
     values.  This creates an nxn matrix where each column has the
     vector of position-specific MS values */
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, ms, unity, 0.0, dep);
  /* Transpose this matrix and add it to itself.  This gives the sum
     of position-specific averages for each pair of positions. */
  gsl_matrix_transpose_memcpy (tmp, dep);
  gsl_matrix_add (dep, tmp);
  /* Divide element-wise this matrix from the all-vs-all MI matrix
     to give the adjusted MI value for each pair of positions */
  gsl_matrix_memcpy (dep_ratio, mi_allvsall);
  gsl_matrix_div_elements (dep_ratio, dep);
  gsl_matrix_free (ms);
  gsl_matrix_free (tmp);
  gsl_matrix_free (dep);
  gsl_matrix_free (unity);
}


double
entropy_weight (gsl_matrix *meas, gsl_matrix *col1_entropies,
                gsl_matrix *col2_entropies)
{
  /* Entropy-weighting: For each matrix element, multiply by a
     weighting factor, which is Hi * Hj * (1 - Hi * Hj) Where Hi and
     Hj are the column entropies corresponding to the matrix position
 */
  gsl_matrix *ent_factor, *tmp;
  ent_factor = gsl_matrix_alloc (col1_entropies->size1, col1_entropies->size2);
  tmp = gsl_matrix_alloc (col1_entropies->size1, col1_entropies->size2);
  gsl_matrix_set_all (tmp, 1.0);
  gsl_matrix_memcpy (ent_factor, col1_entropies);
  gsl_matrix_mul_elements (ent_factor, col2_entropies);
  gsl_matrix_sub (tmp, ent_factor);
  gsl_matrix_mul_elements (ent_factor, tmp);
  gsl_matrix_mul_elements (meas, ent_factor);
  gsl_matrix_free (tmp);
  gsl_matrix_free (ent_factor);
}


double
calc_ewdr (gsl_matrix *mi_allvsall, gsl_matrix *adj_mi,
           gsl_matrix *col1_entropies, gsl_matrix *col2_entropies,
           alignment msa, bool adjust, copts opts, cparams params)
{
  /* Calculate the entropy-weighted dependency ratio, c.f. Tillier et
     al 2003 Bioinformatics */
  calc_dep_ratio (mi_allvsall, adj_mi, msa, adjust, opts, params);
  entropy_weight (adj_mi, col1_entropies, col2_entropies);
}


void
calc_cps (gsl_matrix *mi_allvsall, gsl_matrix *cps)
{
  /*   CPS(i,j)=1/(n-2) * sum(MI(i,k)*MI(k,j), for k != i,j)

     which, for a symmetric MI matrix, can be reinterpreted as

       CPS(i,j)=1/(n-2) * sum(MI(i,k)*MI(k,j), for k != i,j)

     That is, the dot product of row vector i and column vector j,
     minus positions i and j in the respective vectors.  So, this can
     be found via matrix operations:

       CPS = 1/(n-2)(MI * MI - MI)

     where the multiplication is standard matrix multiplication and
     the subtraction is element-wise.
  */
  gsl_matrix *I, *diag, *no_diag;
  I = gsl_matrix_alloc (mi_allvsall->size1, mi_allvsall->size2);
  gsl_matrix_set_identity (I);
  diag = gsl_matrix_alloc (mi_allvsall->size1, mi_allvsall->size2);
  gsl_matrix_memcpy (diag, mi_allvsall);
  gsl_matrix_mul_elements (diag, I);
  no_diag = gsl_matrix_alloc (mi_allvsall->size1, mi_allvsall->size2);
  gsl_matrix_memcpy (no_diag, mi_allvsall);
  gsl_matrix_sub (no_diag, diag);

  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, no_diag, no_diag,
                  0.0, cps);
  gsl_matrix_scale (cps, 1.0/((double) mi_allvsall->size1 - 2.0));
  gsl_matrix_free (I);
  gsl_matrix_free (diag);
  gsl_matrix_free (no_diag);
}


void
calc_ncps (gsl_matrix *mi_allvsall, gsl_matrix *cps, gsl_matrix *adj_mi)
{
  /* NCPS(i,j) = CPS(i,j)/sqrt((1/(n*(n-1)))*sum(CPS(i,j), for all i,j) */
  double nfact;
  gsl_matrix *ncps;
  gsl_vector_view cps_v;
  gsl_matrix *I, *diag, *no_diag;
  ncps = gsl_matrix_alloc (mi_allvsall->size1, mi_allvsall->size2);
  I = gsl_matrix_alloc (cps->size1, cps->size2);
  gsl_matrix_set_identity (I);
  diag = gsl_matrix_alloc (cps->size1, cps->size2);
  gsl_matrix_memcpy (diag, cps);
  gsl_matrix_mul_elements (diag, I);
  no_diag = gsl_matrix_alloc (cps->size1, cps->size2);
  gsl_matrix_memcpy (no_diag, mi_allvsall);
  gsl_matrix_sub (no_diag, diag);
  cps_v = gsl_vector_view_array (no_diag->data, no_diag->size1*no_diag->size2);
  /* Getting the sum of all CPS values by taking half the sum for
     all-vs-all (not including the diagonal) + plus the sum of the
     diagonal */
  nfact = gsl_blas_dasum (&cps_v.vector) / 2.0;
  nfact *= 1.0/((double) cps->size1*((double) cps->size1-1.0));
  nfact = sqrt (nfact);
  gsl_matrix_memcpy (ncps, cps);
  gsl_matrix_scale (ncps, 1.0/nfact);
  gsl_matrix_memcpy (adj_mi, mi_allvsall);
  gsl_matrix_sub (adj_mi, ncps);
  gsl_matrix_free (I);
  gsl_matrix_free (diag);
  gsl_matrix_free (no_diag);
  gsl_matrix_free (ncps);
}


void
calc_amic (gsl_matrix *mic, gsl_matrix *adj_mi, gsl_matrix *col1_entropies,
           gsl_matrix *col2_entropies)
{
  double max_mic, max_emic;
  gsl_matrix *emic;
  gsl_matrix *I, *mic_diag, *mic_no_diag, *emic_diag, *emic_no_diag;
  emic = gsl_matrix_alloc (mic->size1, mic->size2);
  gsl_matrix_memcpy (emic, mic);
  entropy_weight (emic, col1_entropies, col2_entropies);

  I = gsl_matrix_alloc (mic->size1, mic->size2);
  gsl_matrix_set_identity (I);
  mic_diag = gsl_matrix_alloc (mic->size1, mic->size2);
  gsl_matrix_memcpy (mic_diag, mic);
  gsl_matrix_mul_elements (mic_diag, I);
  mic_no_diag = gsl_matrix_alloc (mic->size1, mic->size2);
  gsl_matrix_memcpy (mic_no_diag, mic);
  gsl_matrix_sub (mic_no_diag, mic_diag);
  emic_diag = gsl_matrix_alloc (emic->size1, emic->size2);
  gsl_matrix_memcpy (emic_diag, emic);
  gsl_matrix_mul_elements (emic_diag, I);
  emic_no_diag = gsl_matrix_alloc (emic->size1, emic->size2);
  gsl_matrix_memcpy (emic_no_diag, emic);
  gsl_matrix_sub (emic_no_diag, emic_diag);
  max_mic = gsl_matrix_max (mic_no_diag);
  max_emic = gsl_matrix_max (emic_no_diag);

  gsl_matrix_memcpy (adj_mi, mic);
  gsl_matrix_scale (adj_mi, 1.0/max_mic);
  gsl_matrix_scale (emic, 1.0/max_emic);
  gsl_matrix_add (adj_mi, emic);
  gsl_matrix_scale (adj_mi, 0.5);
}
