/* 
 * mi.h --- Calculate the mutual information between two sites in an
 * alignment
 * 
 * Copyright (C) 2014, 2015 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MI_H
#define MI_H

#include "config.h"

#include "error.h"
#include <errno.h>
#include <math.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_statistics_double.h>
#include "kseq.h"
#include "xmalloc.h"
#include "alignment.h"
#include "aminoacid.h"
#include "probs.h"
#include "entropy.h"
#include "profile.h"
#include "options.h"

double calc_mi (double h1, double h2, double joint_h);
void calc_mi_allvsall (gsl_matrix *joint_entropies, gsl_matrix *col1_entropies,
                       gsl_matrix *col2_entropies, gsl_matrix *mi_allvsall,
                       size_t align_len);
double calc_avg_mi (gsl_vector *pos_avg_mi);
void calc_pos_avg_mi (gsl_matrix *mi_allvsall, size_t align_len,
                      gsl_vector *pos_avg_mi);
void calc_rcw (gsl_matrix *mi_allvsall, gsl_matrix *adj_mi, bool as_published);
void calc_apc (gsl_matrix *mi_allvsall, gsl_matrix *adj_mi,
               gsl_vector *pos_avg_mi, double mi_avg);
void calc_asc (gsl_matrix *mi_allvsall, gsl_matrix *adj_mi,
                 gsl_vector *pos_avg_mi, double mi_avg);
void calc_jen (gsl_matrix *mi_allvsall, gsl_matrix *adj_mi,
                 gsl_matrix *joint_entropies);
void calc_amen (gsl_matrix *mi_allvsall, gsl_matrix *adj_mi,
                gsl_matrix *col1_entropies, gsl_matrix *col2_entropies);
void calc_mmen (gsl_matrix *mi_allvsall, gsl_matrix *adj_mi,
                gsl_matrix *col1_entropies, gsl_matrix *col2_entropies);
double calc_ewdr (gsl_matrix *mi_allvsall, gsl_matrix *adj_mi,
                  gsl_matrix *col1_entropies, gsl_matrix *col2_entropies,
                  alignment msa, bool adjust, copts opts, cparams params);
void calc_cps (gsl_matrix *mi_allvsall, gsl_matrix *cps);
void calc_ncps (gsl_matrix *mi_allvsall, gsl_matrix *cps, gsl_matrix *ncps);
void calc_amic (gsl_matrix *mic, gsl_matrix *adj_mi, gsl_matrix *col1_entropies,
                gsl_matrix *col2_entropies);

#endif
