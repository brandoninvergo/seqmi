/* 
 * options.h --- 
 * 
 * Copyright (C) 2014, 2015 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPTIONS_H
#define OPTIONS_H

#include <stdbool.h>
#include "profile.h"

typedef struct cparams cparams;
typedef struct copts copts;

struct cparams
{
  double sp_lambda;
  double pcmix_a;
  double pcmix_b;
  seq_prof *prf;
  size_t num_vars;
  char res1, res2;
  gsl_matrix **joint_probs;
};

struct copts
{
  bool sp;
  bool bp;
  bool fp;
  bool pp;
  bool marg_prf;
  bool cj;
  bool weight;
  bool add_pc;
  bool as_published;
  double skip_gaps;
};

#endif
