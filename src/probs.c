/* 
 * probs.c --- Build contingency and probability tables
 * 
 * Copyright (C) 2014, 2015, 2016, 2019 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "probs.h"


size_t
contingency_table (alignment msa, size_t pos1, size_t pos2, gsl_matrix *counts,
                   copts opts)
{
  /* Calculate a general contingency table for two positions, counting
     the occurrences of each amino acid in the two positions.  Thus,
     each site is treated as a discrete random variable with 21
     possible values. */
  size_t i, nseqs = 0;
  aminoacid res1, res2;
  gsl_matrix_set_zero (counts);
  for (i=0; i<msa.num_seqs; i++)
    {
      if (pos1 >= msa.length || pos2 >= msa.length)
        error (EXIT_FAILURE, errno, "Residue position is larger than alignment length");
      res1 = alignment_get_aa (msa, i, pos1);
      if (res1 == ERR)
        error (EXIT_FAILURE, errno, "Unknown residue in position %d: %c",
               (int) pos1, alignment_get_char (msa, i, pos1));
      res2 = alignment_get_aa (msa, i, pos2);
      if (res2 == ERR)
        error (EXIT_FAILURE, errno, "Unknown residue in position %d: %c",
               (int) pos2, alignment_get_char (msa, i, pos2));
      if (aa_is_ambiguous (res1) || aa_is_ambiguous (res2))
        continue;
      if (opts.weight)
        gsl_matrix_set (counts, (size_t) res1, (size_t) res2,
                        gsl_matrix_get (counts, (size_t) res1, (size_t) res2) \
                        + gsl_vector_get (msa.weights, i));
      else
        gsl_matrix_set (counts, (size_t) res1, (size_t) res2,
                        gsl_matrix_get (counts, (size_t) res1, (size_t) res2) \
                        + 1.0);
      nseqs++;
    }
  if (opts.weight)
    return 1;
  return nseqs;
}


double
calc_joint_freq_pseudocount (size_t cell1, size_t cell2, cparams params)
{
  aminoacid aa1, aa2;
  if (params.num_vars != 2)
    return aa_freqs_b62[cell1] * aa_freqs_b62[cell2];
  aa1 = char2aa[(size_t) params.res1];
  aa2 = char2aa[(size_t) params.res2];
  if (cell1 == 0 && cell2 == 0)
    return aa_freqs_b62[aa1] * aa_freqs_b62[aa2];
  if (cell1 == 0 && cell2 == 1)
    return aa_freqs_b62[aa1] * (1.0 - aa_freqs_b62[aa2]);
  if (cell1 == 1 && cell2 == 0)
    return (1.0 - aa_freqs_b62[aa1]) * aa_freqs_b62[aa2];
  else
    return (1.0 - aa_freqs_b62[aa1]) * (1.0 - aa_freqs_b62[aa2]);
}


double
calc_joint_blosum_pseudocount (gsl_matrix *counts, size_t nseqs, size_t cell1,
                               size_t cell2, cparams params)
{
  gsl_matrix_view blosum;
  gsl_vector_view aa_freqs;
  size_t i, j;
  double pc = 0.0, q;
  gsl_vector *q_i1, *q_i2, *q_i;
  gsl_vector_view counts_view, blosum_view1, blosum_view2;
  gsl_matrix *counts_cpy;
  blosum = gsl_matrix_view_array (sub_freqs_b62, NUM_AA, NUM_AA);
  aa_freqs = gsl_vector_view_array (aa_freqs_b62, NUM_AA);
  if (params.num_vars != 2)
    {
      counts_cpy = gsl_matrix_alloc (NUM_AA, NUM_AA);
      q_i1 = gsl_vector_alloc (NUM_AA);
      q_i2 = gsl_vector_alloc (NUM_AA);
      gsl_matrix_memcpy (counts_cpy, counts);
      gsl_matrix_scale (counts_cpy, 1.0/(double)nseqs);

      blosum_view1 = gsl_matrix_column (&blosum.matrix, cell1);
      gsl_vector_memcpy (q_i1, &blosum_view1.vector);
      gsl_vector_div (q_i1, &aa_freqs.vector);

      blosum_view2 = gsl_matrix_column (&blosum.matrix, cell2);
      gsl_vector_memcpy (q_i2, &blosum_view2.vector);
      gsl_vector_div (q_i2, &aa_freqs.vector);

      for (i=0; i<NUM_AA; i++)
        {
          counts_view = gsl_matrix_column (counts_cpy, i);
          gsl_vector_mul (&counts_view.vector, q_i1);
          counts_view = gsl_matrix_row (counts_cpy, i);
          gsl_vector_mul (&counts_view.vector, q_i2);
        }
      for (i=0; i<NUM_AA; i++)
        {
          counts_view = gsl_matrix_row (counts_cpy, i);
          pc += gsl_blas_dasum (&counts_view.vector);
        }
      gsl_matrix_free (counts_cpy);
      gsl_vector_free (q_i1);
      gsl_vector_free (q_i2);
    }
  else
    {
      q_i = gsl_vector_alloc (2);
      gsl_vector_set_zero (q_i);
      for (i=0; i<NUM_AA; i++)
        {
          for (j=0; j<NUM_AA; j++)
            {
              q = gsl_matrix_get (&blosum.matrix, i, j);
              if (i == char2aa[(size_t) params.res1])
                gsl_vector_set (q_i, 0, gsl_vector_get (q_i, 0) + q);
              else if (i == char2aa[(size_t) params.res2])
                gsl_vector_set (q_i, 1, gsl_vector_get (q_i, 1) + q);
            }
        }
      if (cell1 == 0 && cell2 == 0)
        pc = ((gsl_matrix_get (counts, 0, 0) / nseqs)
              * gsl_vector_get (q_i, 0)
              * gsl_vector_get (q_i, 1));
      else if (cell1 == 0 && cell2 == 1)
        pc = ((gsl_matrix_get (counts, 0, 0) / nseqs)
              * gsl_vector_get (q_i, 0)
              * (1.0 - gsl_vector_get (q_i, 1)));
      else if (cell1 == 1 && cell2 == 0)
        pc = ((gsl_matrix_get (counts, 0, 0) / nseqs)
              * (1.0 - gsl_vector_get (q_i, 0))
              * gsl_vector_get (q_i, 1));
      else
        pc = ((gsl_matrix_get (counts, 0, 0) / nseqs)
              * (1.0 - gsl_vector_get (q_i, 0))
              * (1.0 - gsl_vector_get (q_i, 1)));
      gsl_vector_free (q_i);
    }
  return pc;
}


void
calc_joint_prf_pseudocounts (gsl_matrix *pseudocounts, size_t pos1, size_t pos2,
                             copts opts, cparams params)
{
  gsl_matrix_view pos1_prf, pos2_prf;
  pos1_prf = gsl_matrix_submatrix (params.prf->p, pos1, 0, 1, NUM_AA);
  pos2_prf = gsl_matrix_submatrix (params.prf->p, pos2, 0, 1, NUM_AA);
  gsl_blas_dgemm (CblasTrans, CblasNoTrans, 1.0, &pos1_prf.matrix, &pos2_prf.matrix,
                  0.0, pseudocounts);
}


void
get_joint_pseudocounts (gsl_matrix *counts, gsl_matrix *pseudocounts,
                        size_t nseqs, size_t pos1, size_t pos2, copts opts,
                        cparams params)
{
  size_t i, j;
  double pc;
  if (opts.pp)
    {
      calc_joint_prf_pseudocounts (pseudocounts, pos1, pos2, opts, params);
      return;
    }
  for (i=0; i<params.num_vars; i++)
    {
      for (j=0; j<params.num_vars; j++)
        {
          if (opts.fp)
            pc = calc_joint_freq_pseudocount (i, j, params);
          else if (opts.bp)
            pc = calc_joint_blosum_pseudocount (counts, nseqs, i, j, params);
          gsl_matrix_set (pseudocounts, i, j, pc);
        }
    }
}


double
matrix_sum (gsl_matrix *m)
{
  gsl_matrix *ones = gsl_matrix_alloc (m->size1, 1);
  gsl_matrix *colsum = gsl_matrix_alloc (1, m->size2);
  gsl_vector_view ones_v = gsl_matrix_column (ones, 0);
  gsl_vector_view colsum_v;
  double sum;

  gsl_matrix_set_all (ones, 1.0);
  gsl_blas_dgemm (CblasTrans, CblasNoTrans, 1.0, ones, m, 0.0, colsum);
  colsum_v = gsl_matrix_row (colsum, 0);
  gsl_blas_ddot (&colsum_v.vector, &ones_v.vector, &sum);

  gsl_matrix_free (colsum);
  gsl_matrix_free (ones);
  return sum;
}


void
calc_joint_probs (gsl_matrix *counts, alignment msa, size_t nseqs, size_t pos1,
                  size_t pos2, gsl_matrix *joint_probs, copts opts,
                  cparams params)
{
  double tau, neff;
  gsl_matrix *pseudocounts = NULL;
  double prob_sum;

  if (opts.add_pc)
    {
      pseudocounts = gsl_matrix_alloc (params.num_vars, params.num_vars);
      get_joint_pseudocounts (counts, pseudocounts, nseqs, pos1, pos2, opts,
                              params);
      neff = gsl_matrix_get (msa.joint_neff, pos1, pos2);
      tau = (1.0 + params.pcmix_b) / (exp (params.pcmix_a * neff) + params.pcmix_b);
    }
  else
    tau = 0.0;
  gsl_matrix_memcpy (joint_probs, counts);
  if (opts.sp)
    {
      gsl_matrix_add_constant (joint_probs, params.sp_lambda);
    }
  else
    {
      if (tau)
        gsl_matrix_scale (joint_probs, 1.0-tau);
      if (tau && opts.add_pc)
        {
          gsl_matrix_scale (pseudocounts, tau);
          gsl_matrix_add (joint_probs, pseudocounts);
        }
    }
  prob_sum = matrix_sum (joint_probs);
  gsl_matrix_scale (joint_probs, 1.0/prob_sum);
  if (opts.cj)
    {
      constrain_joint_probs (joint_probs, pos1, pos2, params);
    }
  if (pseudocounts)
    gsl_matrix_free (pseudocounts);
}


void
calc_marg_probs (gsl_matrix *joint_probs, gsl_vector *pos1_marg_probs,
                 gsl_vector *pos2_marg_probs, size_t pos1, size_t pos2,
                 copts opts, cparams params)
{
  gsl_vector_view vv1, vv2;
  gsl_vector *unityv;
  gsl_vector_set_zero (pos1_marg_probs);
  gsl_vector_set_zero (pos2_marg_probs);
  if (opts.marg_prf)
    {
      vv1 = gsl_matrix_row (params.prf->p, pos1);
      vv2 = gsl_matrix_row (params.prf->p, pos2);
      gsl_vector_memcpy (pos1_marg_probs, &vv1.vector);
      gsl_vector_memcpy (pos2_marg_probs, &vv2.vector);
    }
  else
    {
      unityv = gsl_vector_alloc(params.num_vars);
      gsl_vector_set_all(unityv, 1.0);
      gsl_blas_dgemv(CblasNoTrans, 1.0, joint_probs, unityv, 0.0, pos1_marg_probs);
      gsl_blas_dgemv(CblasTrans, 1.0, joint_probs, unityv, 0.0, pos2_marg_probs);
      gsl_vector_free(unityv);
    }
}
