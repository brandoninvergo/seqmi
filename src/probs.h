/* 
 * probs.h --- Build contingency and probability tables
 * 
 * Copyright (C) 2014, 2015 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONTINGENCY_H
#define CONTINGENCY_H

#include <stdbool.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include "aminoacid.h"
#include "alignment.h"
#include "profile.h"
#include "constrain.h"
#include "blosum.h"

size_t contingency_table (alignment msa, size_t pos1, size_t pos2,
                          gsl_matrix *counts, copts opts);
size_t binomial_contingency_table (alignment msa, size_t pos1, size_t pos2,
                                 gsl_matrix *counts, copts opts,
                                 cparams params);
void calc_joint_probs (gsl_matrix *counts, alignment msa, size_t nseqs,
                       size_t pos1, size_t pos2, gsl_matrix *joint_probs,
                       copts opts, cparams params);
void calc_marg_probs (gsl_matrix *joint_probs, gsl_vector *pos1_marg_probs,
                      gsl_vector *pos2_marg_probs, size_t pos, size_t pos2,
                      copts opts, cparams params);

#endif
