/* 
 * profile.c --- Read sequence profiles
 * 
 * Copyright (C) 2014, 2016 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "profile.h"

seq_prof*
read_profile (char *prf_file, const size_t *msa_cols, size_t align_len)
{
  FILE *prf_handle;
  char *line;
  long int aa_prob, neff;
  size_t j, nsites = 0, nstates = 0, n = 0;
  size_t cur_msa_col = 0;
  ssize_t len;
  size_t nchr = 128;
  char *tok = NULL;
  seq_prof *prf;
  aminoacid aa_index[NUM_AA];
  prf_handle = fopen ((const char *)prf_file, "r");
  if (!prf_handle)
    error (EXIT_FAILURE, errno, "Can't open profile file");
  line = (char *)xmalloc (nchr * sizeof (char));
  while (!feof_unlocked (prf_handle))
    {
      len = getline (&line, &nchr, prf_handle) >= 0;
      if (len <= 0)
        break;
      tok = strtok (line, "\t");
      if (!tok)
        {
          free (line);
          fclose (prf_handle);
          error (EXIT_FAILURE, errno, "Can't tokenize line: %s", line);
        }
      if (!strcmp (tok, "LENG"))
        {
          tok = strtok (NULL, "\t");
          if (!tok)
            {
              free (line);
              fclose (prf_handle);
              error (EXIT_FAILURE, errno, "No profile length given");
            }
          nsites = (size_t) strtol (tok, NULL, 0);
          if (nsites <= 0)
            {
              free (line);
              fclose (prf_handle);
              error (EXIT_FAILURE, errno, "Cannot determine profile length");
            }
          if (nsites < align_len)
            {
              free (line);
              fclose (prf_handle);
              error (EXIT_FAILURE, errno, "Profile too short");
            }
        }
      else if (!strcmp (tok, "ALPH"))
        {
          tok = strtok (NULL, "\t");
          if (!tok)
            {
              free (line);
              fclose (prf_handle);
              error (EXIT_FAILURE, errno, "No profile alphabet length given");
            }
          nstates = (size_t) strtol (tok, NULL, 0);
          if (nstates <= 0)
            {
              free (line);
              fclose (prf_handle);
              error (EXIT_FAILURE, errno,
                     "Cannot determine profile alphabet length");
            }
        }
      else if (!strcmp (tok, "COUNTS"))
        {
          if (nstates == 20)
            {
              for (j=0; j<nstates; j++)
                {
                  tok = strtok (NULL, "\t");
                  if (!tok)
                    {
                      free (line);
                      fclose (prf_handle);
                      error (EXIT_FAILURE, errno,
                             "Too few columns in COUNTS row");
                    }
                  aa_index[j] = char2aa[(size_t) tok[0]];
                }
            }
          break;
        }
    }
  if (nsites == 0 || nstates == 0)
    {
      if (line)
        free (line);
      if (tok)
        free (tok);
      fclose (prf_handle);
      error (EXIT_FAILURE, errno, "Could not read sequence profile");
    }
  prf = (seq_prof *)xmalloc (sizeof (seq_prof));
  prf->p = gsl_matrix_calloc (align_len, nstates);
  prf->neff = gsl_vector_calloc (align_len);
  while (!feof_unlocked (prf_handle))
    {
      len = getline (&line, &nchr, prf_handle);
      if (len <= 0)
        {
          if ((size_t) n + 1 < prf->p->size1)
            {
              if (line)
                free (line);
              if (tok)
                free (tok);
              destroy_profile (prf);
              fclose (prf_handle);
              error (EXIT_FAILURE, errno, "Profile file truncated");
            }
          break;
        }
      tok = strtok (line, "\t");
      if (!strcmp (tok, "//\n"))
          break;
      n = (size_t) strtol (tok, NULL, 0);
      if (n <= 0)
        {
          if (line)
            free (line);
          if (tok)
            free (tok);
          destroy_profile (prf);
          fclose (prf_handle);
          error (EXIT_FAILURE, errno, "Strange profile row number: %ld", n);
        }
      n--;
      if (msa_cols)
        {
          if (n != msa_cols[cur_msa_col])
            continue;
          n = cur_msa_col++;
        }
      for (j=0; j<nstates; j++)
        {
          tok = strtok (NULL, "\t");
          if (!tok)
            {
              if (line)
                free (line);
              destroy_profile (prf);
              fclose (prf_handle);
              error (EXIT_FAILURE, errno,
                     "Too few columns in profile row %ld", n+1);
            }
          if (!strcmp (tok, "*"))
            {
              continue;
            }
          aa_prob = strtol (tok, NULL, 0);
          gsl_matrix_set (prf->p, n, aa_index[j],
                          exp2 ((double) aa_prob / -1000.0));
        }
      tok = strtok (NULL, "\t");
      if (!tok)
        {
          if (line)
            free (line);
          destroy_profile (prf);
          fclose (prf_handle);
          error (EXIT_FAILURE, errno,
                 "N_eff column missing in profile row %ld", n+1);
        }
      neff = strtol (tok, NULL, 0);
      gsl_vector_set (prf->neff, n, exp2 (neff / -1000.0));
      if (cur_msa_col >= align_len)
        break;
    }
  free (line);
  fclose (prf_handle);
  return prf;
}


void
destroy_profile (seq_prof *prf)
{
  gsl_matrix_free (prf->p);
  gsl_vector_free (prf->neff);
  free (prf);
  return;
}
