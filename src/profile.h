/* 
 * profile.h --- Read sequence profiles
 * 
 * Copyright (C) 2014, 2016 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef PROFILE_H
#define PROFILE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "error.h"
#include <errno.h>
#include <string.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include "xmalloc.h"
#include "aminoacid.h"

typedef struct seq_prof seq_prof;

struct seq_prof
{
  gsl_matrix *p;
  gsl_vector *neff;
};

seq_prof* read_profile (char *prf_file, const size_t *msa_cols, size_t align_len);
void destroy_profile (seq_prof *prf);

#endif
