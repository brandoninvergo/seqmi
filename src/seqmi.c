 /* 
 * seqmi.c --- Calculate mutual information on sites in aligned
 * sequences analysis
 * 
 * Copyright (C) 2014, 2015, 2016 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdlib.h>
#include <stdio.h>
#include "argp.h"
#include <string.h>
#include "error.h"
#include <errno.h>
#include <stdbool.h>
#include <gsl/gsl_matrix.h>
#include <omp.h>

#include "xmalloc.h"
#include "alignment.h"
#include "mi.h"
#include "probs.h"
#include "entropy.h"
#include "aminoacid.h"
#include "profile.h"
#include "options.h"

#define CHUNKSIZE 100


const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

static char doc[] =
  "seqmi -- compute mutual information between sites in aligned biological sequences"
  "\v"
  "This program calculates the mutual information between sites in aligned "
  "biological sequences.  An ALIGNMENT in FASTA format must be provided.  For "
  "more information on the methods, see the manual (run 'info seqmi').";

static char args_doc[] = "ALIGNMENT";

static struct argp_option options[] = {
  {0,              0,   0,          0, "Site Selection", 1},
  {"skip-gaps",    'g', "THRESHOLD",OPTION_ARG_OPTIONAL,
                                       "Do not include columns with alignment "
                                       "gaps.  Optional: include columns with "
                                       "up to THRESHOLD (0 </ T <= 1) percent "
                                       "rows with gaps", 0},
  {0,              0,   0,          0, "Mutual Information Adjustment/Correction", 2},
  {"apc",          'a', 0,          0, "Perform an average product correction "
                                       "(APC)", 2},
  {"asc",          'c', 0,          0, "Perform an average sum correction "
                                       "(ASC)", 2},
  {"adj-ewdr",     'd', 0,          0, "Perform an entropy-weighted dependency "
                                       "ratio normalization, adjusting for gaps", 0},
  {"ewdr",         140, 0,          0, "Perform an entropy-weighted dependency "
                                       "ratio normalization", 0},
  {"rcw",          'w', 0,          0, "Perform row-column weighting (original)", 0},
  {"rcw-fixed",    141, 0,          0, "Perform row-column weighting (fixed)", 0},
  {"jen",          'j', 0,          0, "Perform joint-entropy normalization", 0},
  {"amen",         'm', 0,          0, "Perform additive marginal-entropy "
                                       "normalization (aMEN)", 0},
  {"mmen",         'u', 0,          0, "Perform multiplicative marginal-entropy "
                                       "normalization (mMEN)", 0},
  {"ncps",         'n', 0,          0, "Perform normalized co-evolutionary pattern "
                                       "similarity (NCPS) correction", 0},
  {"amic",         'i', 0,          0, "Calculate aMIc", 0},
  {0,              0,   0,          0, "Probability Calculation", 3},
  {"joint-probs",  150, "METHOD",   0, "Specify the method of joint probability "
                                       "calculation: 'emp' (empirical; "
                                       "default), 'sp' (simple pseudocount), 'fp' "
                                       "(frequency-based pseudocount), 'bp' "
                                       "(BLOSUM62-based pseudocount), or 'pp' "
                                       "(profile-based pseudocount)", 0},
  {"marg-probs",   151, "METHOD",   0, "Specify the method of marginal probability "
                                       "calculation: 'jp' (derived from joint "
                                       "probabilities; default), 'prf' "
                                       "(profile-based)", 0},
  {"constrain-joint-probs", 152, 0, 0, "Constrain join probabilities by profile-"
                                       "based marginal probabilities", 0},
  {"weight-sequences",  153,     0, 0, "Weight sequences according to the Henikoff "
                                       "weighting scheme (for empirical probability "
                                       "calculations)", 0},
  {"precalc-joint-probs", 154,   0, 0, "Pre-calculate joint probabilities.  This "
                                       "will speed up the program in some cases "
                                       "at the expense of memory usage (WARNING: "
                                       "on long alignments, this can easily use "
                                       "several gigabytes of RAM)", 0},
  {0,              0,   0,          0, "Parameters", 4},
  {"sp-lambda",    160, "VALUE",    0, "Correction parameter for simple "
                                       "pseudocounts (default 0.05)", 0},
  {"pcmix-a",         161, "VALUE",    0, "Parameter for pseudocount mixture "
                                       "(default 0.5)", 0},
  {"pcmix-b",         162, "VALUE",    0, "Parameter for pseudocount mixture "
                                       "(default 1.0)", 0},
  {0,              0,   0,          0, "Output", 5},
  {"print-entropies",170, 0,        0, "Include column and joint entropies in the "
                                       "output", 0},
  {"print-perc-gaps",171, 0,        0, "Include gap percentages in the output", 0},
  {0,              0,   0,          0, "Other", 6},
  {"profile",      'p', "PROFILE",  0, "A file containing a sequence profile "
                                       "produced by the csbuild program of "
                                       "CS-BLAST.  Required for profile-based "
                                       "probability methods", 0},
  {0,              0,   0,          0, "More Information", -1},
  {"references",   180, 0,          0, "Print a list of primary references "
                                       "describing the methods used in this "
                                       "software and then exit.", 0},
  {0}
};

struct arguments
{
  char *args[1];
  char *profile;
  char *jprob;
  char *mprob;
  bool rcw;
  bool rcw_fixed;
  bool apc;
  bool asc;
  bool jen;
  bool amen;
  bool mmen;
  bool ewdr;
  bool adj_ewdr;
  bool ncps;
  bool amic;
  bool cj;
  bool precalc_jp;
  bool weight;
  bool print_entropy;
  bool print_gaps;
  double skip_gaps;
  double sp_lambda;
  double pcmix_a;
  double pcmix_b;
};

int
print_references (void)
{
  puts ("For more information on the methods implemented in this software see the\n"
          "following publications:");
  puts ("\nSimple pseudocount adjustment:\n\t"
          "Buslje CM, et al. 2009. Bioinformatics 25(9):1125-1131\n"
          "\tdoi:10.1093/bioinformatics/btp135");
  puts ("\nRow-column weighting (RCW):\n\t"
          "Gouveia-Oliveira R and Pedersen AG. 2007. Algorithms for \n"
          "\tMolecular Biology 2:12\n"
          "\tdoi:10.1186/1748-7188-2-12");
  puts ("\nAverage product correction (APC) and average sum correction (ASC):\n\t"
          "Dunn SD, Wahl LM and Gloor GB. 2008. Bioinformatics \n"
          "\t24(3):333-340\n"
          "\tdoi:10.1093/bioinformatics/btm604");
  puts ("\nJoint-entropy normalization (JEN):\n\t"
          "Martin LC, Gloor GB, Dunn SD, and Wahl LM. 2005.\n"
          "\tBioinformatics 21(22):4116-4124\n"
          "\tdoi:10.1093/bioinformatics/bti671");
  puts ("\nEntropy-weighted dependency ratio (EWDR):\n\t"
          "Tillier ERM, LUi TWH. 2003. Bioinformatics 19(6):750-755\n"
          "\tdoi:10.1093/bioinformatics/btg072");
  puts ("\nNormalized co-evolutionary pattern similarity (NCPS) and aMIc: \n\t"
          "Lee BC and Kim D. 2009. Bioinformatics 25(19):2506-2513\n"
          "\tdoi:10.1093/bioinformatics/btp455");
  exit (EXIT_SUCCESS);
}

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
  switch (key)
    {
    case 'a':
      arguments->apc = true;
      break;
    case 'c':
      arguments->asc = true;
      break;
    case 'd':
      arguments->adj_ewdr = true;
      break;
    case 'g':
      if (!arg || strlen (arg) == 0)
        arguments->skip_gaps = 0.0;
      else
        {
          arguments->skip_gaps = strtod (arg, NULL);
          if (!arguments->skip_gaps || arguments->skip_gaps > 1.0 ||
              arguments->skip_gaps < 0.0)
            argp_usage (state);
        }
      break;
    case 'i':
      arguments->amic = true;
      break;
    case 'j':
      arguments->jen = true;
      break;
    case 'm':
      arguments->amen = true;
      break;
    case 'n':
      arguments->ncps = true;
      break;
    case 'p':
      if (strlen (arg) == 0)
        argp_usage (state);
      arguments->profile = arg;
      break;
    case 'u':
      arguments->mmen = true;
      break;
    case 'w':
      arguments->rcw = true;
      break;
    case 140:
      arguments->ewdr = true;
      break;
    case 141:
      arguments->rcw_fixed = true;
      break;
    case 150:
      if (strlen (arg) == 0)
        argp_usage (state);
      arguments->jprob = arg;
      break;
    case 151:
      if (strlen (arg) == 0)
        argp_usage (state);
      arguments->mprob = arg;
      break;
    case 152:
      arguments->cj = true;
      break;
    case 153:
      arguments->weight = true;
      break;
    case 154:
      arguments->precalc_jp = true;
      break;
    case 160:
      if (!arg || strlen (arg) == 0)
        argp_usage (state);
      else
        {
          arguments->sp_lambda = strtod (arg, NULL);
          if (!arguments->sp_lambda)
              argp_usage (state);
        }
      break;
    case 161:
      if (!arg || strlen (arg) == 0)
        argp_usage (state);
      else
        {
          arguments->pcmix_a = strtod (arg, NULL);
          if (!arguments->pcmix_a)
              argp_usage (state);
        }
      break;
    case 162:
      if (!arg || strlen (arg) == 0)
        argp_usage (state);
      else
        {
          arguments->pcmix_b = strtod (arg, NULL);
          if (!arguments->pcmix_b)
              argp_usage (state);
        }
      break;
    case 170:
      arguments->print_entropy = true;
      break;
    case 171:
      arguments->print_gaps = true;
      break;
    case 180:
      print_references ();
      break;
    case ARGP_KEY_ARG:
      if (state->arg_num >= 1)
        argp_usage (state);
      arguments->args[state->arg_num] = arg;
      break;
    case ARGP_KEY_END:
      if (state->arg_num < 1)
        argp_usage (state);
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};

char *fail_msg = NULL;


void
init_args (struct arguments *arguments)
{
  arguments->profile = NULL;
  arguments->jprob = "emp";
  arguments->mprob = "jp";
  arguments->sp_lambda = 0.05;
  arguments->pcmix_a = 0.5;
  arguments->pcmix_b = 1.0;
  arguments->apc = false;
  arguments->asc = false;
  arguments->rcw = false;
  arguments->rcw_fixed = false;
  arguments->jen = false;
  arguments->mmen = false;
  arguments->amen = false;
  arguments->ewdr = false;
  arguments->adj_ewdr = false;
  arguments->ncps = false;
  arguments->amic = false;
  arguments->cj = false;
  arguments->precalc_jp = false;
  arguments->weight = false;
  arguments->skip_gaps = -1.0;
  arguments->print_entropy = false;
  arguments->print_gaps = false;
}


void
set_options (struct arguments arguments, copts *opts, cparams *params)
{
  /* Set internal options and parameters */
  if (!strcmp (arguments.mprob, "prf"))
    opts->marg_prf = true;
  if (!strcmp (arguments.jprob, "sp"))
    {
      opts->sp = true;
      params->sp_lambda = arguments.sp_lambda;
    }
  else if (!strcmp (arguments.jprob, "fp"))
    {
      opts->fp = true;
      params->pcmix_a = arguments.pcmix_a;
      params->pcmix_b = arguments.pcmix_b;
    }
  else if (!strcmp (arguments.jprob, "bp"))
    {
      opts->bp = true;
      params->pcmix_a = arguments.pcmix_a;
      params->pcmix_b = arguments.pcmix_b;
    }
  else if (!strcmp (arguments.jprob, "pp"))
    {
      opts->pp = true;
      params->pcmix_a = arguments.pcmix_a;
      params->pcmix_b = arguments.pcmix_b;
    }
  opts->add_pc = opts->fp || opts->bp || opts->pp;
  opts->cj = arguments.cj;
  opts->weight = arguments.weight;
  params->num_vars = NUM_AA;
  opts->skip_gaps = arguments.skip_gaps;
}


size_t
build_seq2align (kseq_t *ref_align_seq, size_t *seq2align)
{
  size_t i, j = 0;
  for (i=0; i<ref_align_seq->seq.l; i++)
    {
      if (ref_align_seq->seq.s[i] != '-')
        {
          seq2align[j++] = i;
        }
    }
  return j;
}


void
check_args (struct arguments arguments)
{
  if (strcmp (arguments.jprob, "emp") && strcmp (arguments.jprob, "pp") &&
      strcmp (arguments.jprob, "sp") && strcmp (arguments.jprob, "fp") &&
      strcmp (arguments.jprob, "bp"))
    error (EXIT_FAILURE, errno,
           "Joint probability method must be one of: emp, sp, fp, bp, pp");
  if (!strcmp (arguments.jprob, "pp") && !arguments.profile)
    error (EXIT_FAILURE, errno,
           "A sequence profile file must be specified to use profile-based "
           "pseudocounts");
  if (!strcmp (arguments.mprob, "prf") && !arguments.profile)
    error (EXIT_FAILURE, errno,
           "A sequence profile file must be specified to use profile-based "
           "marginal probabilities");
  if (strcmp (arguments.mprob, "jp") && strcmp (arguments.mprob, "prf"))
    error (EXIT_FAILURE, errno,
           "Marginal probability method must be one of: jp, prf");
  if (arguments.sp_lambda < 0.0)
    error (EXIT_FAILURE, errno,
           "Simple pseudocount parameter lambda cannot be negative");
  if (arguments.pcmix_a < 0.0)
    error (EXIT_FAILURE, errno,
           "Pseudocount mixture parameter a cannot be negative");
  if (arguments.pcmix_b < 0.0)
    error (EXIT_FAILURE, errno,
           "Pseudocount mixture parameter b cannot be negative");
  if (arguments.cj && strcmp (arguments.mprob, "prf"))
    error (EXIT_FAILURE, errno, "Joint probability constraint requires that the "
           "marginal probabilities be profile-based");
}


void
append_results_column (gsl_matrix *results, gsl_matrix *new_results, size_t col_num)
{
  gsl_vector_view results_col;
  results_col = gsl_vector_view_array (new_results->data,
                                       new_results->size1*new_results->size2);
  gsl_matrix_set_col (results, col_num, (const gsl_vector *)&results_col.vector);
}


size_t
main_loop (alignment msa, struct arguments arguments, size_t num_sites,
           copts opts, cparams params, gsl_matrix **results)
{
  size_t num_adj, num_col, num_row, res_col;
  double mi_avg;
  gsl_matrix *joint_entropies = NULL;
  gsl_matrix *cps = NULL, *ncps = NULL, *mi_allvsall = NULL, *adj_mi = NULL;
  gsl_matrix *col1_entropies = NULL, *col2_entropies = NULL;
  gsl_vector *pos_avg_mi = NULL;
  gsl_vector_view results_col;
  int chunk = CHUNKSIZE;

  num_adj = arguments.apc + arguments.asc + arguments.rcw + arguments.rcw_fixed \
    + arguments.jen + arguments.mmen + arguments.amen + arguments.ewdr \
    + arguments.adj_ewdr + arguments.ncps + arguments.amic;
  num_col = 1 + arguments.print_entropy*3 + arguments.print_gaps*2 + num_adj;
  num_row = num_sites * num_sites;
  /* *results = gsl_matrix_alloc ((num_sites*(num_sites-1))/2, num_col); */
  *results = gsl_matrix_alloc (num_row, num_col);
  gsl_matrix_set_all (*results, GSL_NAN);
  res_col = 0;

  if (num_adj == 0)
    omp_set_num_threads (1);
  else
    adj_mi = gsl_matrix_alloc (msa.length, msa.length);

  joint_entropies = gsl_matrix_alloc (msa.length, msa.length);
  col1_entropies = gsl_matrix_alloc (msa.length, msa.length);
  col2_entropies = gsl_matrix_alloc (msa.length, msa.length);
  calc_entropy_allvsall (joint_entropies, col1_entropies, col2_entropies, msa,
                         opts, params);
  mi_allvsall = gsl_matrix_alloc (msa.length, msa.length);
  calc_mi_allvsall (joint_entropies, col1_entropies, col2_entropies, mi_allvsall,
                    msa.length);
  results_col = gsl_vector_view_array (mi_allvsall->data,
                                       mi_allvsall->size1*mi_allvsall->size2);
  gsl_matrix_set_col (*results, res_col++,
                      (const gsl_vector *)&results_col.vector);

  if (arguments.apc || arguments.asc)
    {
      pos_avg_mi = gsl_vector_alloc (msa.length);
      calc_pos_avg_mi (mi_allvsall, msa.length, pos_avg_mi);
      mi_avg = calc_avg_mi (pos_avg_mi);
      if (arguments.apc)
        {
          gsl_matrix_set_zero (adj_mi);
          calc_apc (mi_allvsall, adj_mi, pos_avg_mi, mi_avg);
          append_results_column (*results, adj_mi, res_col++);
        }
      if (arguments.asc)
        {
          gsl_matrix_set_zero (adj_mi);
          calc_asc (mi_allvsall, adj_mi, pos_avg_mi, mi_avg);
          append_results_column (*results, adj_mi, res_col++);
        }
      gsl_vector_free (pos_avg_mi);
    }
  if (arguments.rcw)
    {
      gsl_matrix_set_zero (adj_mi);
      calc_rcw (mi_allvsall, adj_mi, true);
      append_results_column (*results, adj_mi, res_col++);
    }
  if (arguments.rcw_fixed)
    {
      gsl_matrix_set_zero (adj_mi);
      calc_rcw (mi_allvsall, adj_mi, false);
      append_results_column (*results, adj_mi, res_col++);
    }
  if (arguments.jen)
    {
      gsl_matrix_set_zero (adj_mi);
      calc_jen (mi_allvsall, adj_mi, joint_entropies);
      append_results_column (*results, adj_mi, res_col++);
    }
  if (arguments.amen)
    {
      gsl_matrix_set_zero (adj_mi);
      calc_amen (mi_allvsall, adj_mi, col1_entropies, col2_entropies);
      append_results_column (*results, adj_mi, res_col++);
    }
  if (arguments.mmen)
    {
      gsl_matrix_set_zero (adj_mi);
      calc_mmen (mi_allvsall, adj_mi, col1_entropies, col2_entropies);
      append_results_column (*results, adj_mi, res_col++);
    }
  if (arguments.ewdr)
    {
      gsl_matrix_set_zero (adj_mi);
      calc_ewdr (mi_allvsall, adj_mi, col1_entropies, col2_entropies, msa,
                 false, opts, params);
      append_results_column (*results, adj_mi, res_col++);
    }
  if (arguments.adj_ewdr)
    {
      gsl_matrix_set_zero (adj_mi);
      calc_ewdr (mi_allvsall, adj_mi, col1_entropies, col2_entropies, msa,
                 true, opts, params);
      append_results_column (*results, adj_mi, res_col++);
    }

  if (arguments.ncps || arguments.amic)
    {
      cps = gsl_matrix_alloc (msa.length, msa.length);
      ncps = gsl_matrix_alloc (msa.length, msa.length);
      calc_cps (mi_allvsall, cps);
      calc_ncps (mi_allvsall, cps, ncps);
      if (arguments.ncps)
        {
          gsl_matrix_memcpy (adj_mi, ncps);
          append_results_column (*results, adj_mi, res_col++);
        }
      if (arguments.amic)
        {
          gsl_matrix_set_zero (adj_mi);
          calc_amic (ncps, adj_mi, col1_entropies, col2_entropies);
          append_results_column (*results, adj_mi, res_col++);
        }
    }
  gsl_matrix_free (joint_entropies);
  gsl_matrix_free (col1_entropies);
  gsl_matrix_free (col2_entropies);
  gsl_matrix_free (mi_allvsall);
  if (num_adj > 0)
    gsl_matrix_free (adj_mi);
  if ((arguments.ncps || arguments.amic))
    {
      if (cps)
        gsl_matrix_free (cps);
      if (ncps)
        gsl_matrix_free (ncps);
    }
  return num_sites;
}


char *
extend_row (char **row, size_t *row_len, char *curp, const char* to_add,
            size_t add_len)
{
  char *newp;
  *row_len += add_len;
  newp = (char *) xrealloc (*row, (*row_len+1) * sizeof (char));
  curp = newp + (curp - *row);
  *row = newp;
  curp = mempcpy (curp, to_add, add_len * sizeof (char));
  return curp;
}



void
print_results (struct arguments arguments, gsl_matrix *results,
               size_t num_sites, alignment msa, copts opts)
{
  size_t i, j, num_adj, num_col, rlen, chunklen;
  size_t pos1, pos2, pos1_orig, pos2_orig;
  double result;
  char *row, *rp, *chunk;
  num_adj = arguments.apc + arguments.asc + arguments.rcw + arguments.rcw_fixed
    + arguments.jen + arguments.mmen + arguments.amen + arguments.ewdr \
    + arguments.adj_ewdr + arguments.ncps + arguments.amic;
  num_col = 1 + arguments.print_entropy*3 + arguments.print_gaps*2 + num_adj;
  /* Output header row */
  rlen = 13;
  row = (char *)xmalloc (rlen * sizeof(char));
  rp = mempcpy (row, "pos1\tpos2", 9);
  if (arguments.print_gaps)
    rp = extend_row (&row, &rlen, rp, "\tperc.gaps1\tperc.gaps2", 22);
  if (arguments.print_entropy)
    rp = extend_row (&row, &rlen, rp, "\th1\th2\tjoint.h", 14);
  rp = mempcpy (rp, "\tmi", 3);
  if (arguments.apc)
    rp = extend_row (&row, &rlen, rp, "\tapc", 4);
  if (arguments.asc)
    rp = extend_row (&row, &rlen, rp, "\tasc", 4);
  if (arguments.rcw)
    rp = extend_row (&row, &rlen, rp, "\trcw", 4);
  if (arguments.rcw_fixed)
    rp = extend_row (&row, &rlen, rp, "\trcw.fixed", 10);
  if (arguments.jen)
    rp = extend_row (&row, &rlen, rp, "\tjen", 4);
  if (arguments.amen)
    rp = extend_row (&row, &rlen, rp, "\tamen", 5);
  if (arguments.mmen)
    rp = extend_row (&row, &rlen, rp, "\tmmen", 5);
  if (arguments.ewdr)
    rp = extend_row (&row, &rlen, rp, "\tewdr", 5);
  if (arguments.adj_ewdr)
    rp = extend_row (&row, &rlen, rp, "\tadj.ewdr", 9);
  if (arguments.ncps)
    rp = extend_row (&row, &rlen, rp, "\tncps", 5);
  if (arguments.amic)
    rp = extend_row (&row, &rlen, rp, "\tamic", 5);
  *rp++ = '\0';
  puts (row);
  free (row);
  for (i=0; i<num_sites*num_sites; i++)
    {
      pos1 = i / num_sites;
      pos2 = i % num_sites;
      if (msa.orig_cols)
        {
          pos1_orig = msa.orig_cols[pos1];
          pos2_orig = msa.orig_cols[pos2];
        }
      else
        {
          pos1_orig = pos1;
          pos2_orig = pos2;
        }
      if (asprintf (&chunk, "%zd\t%zd", pos1_orig+1, pos2_orig+1) < 0)
        error (EXIT_FAILURE, errno, "Memory exhausted");
      rlen = strlen (chunk);
      row = (char *)xmalloc ((rlen+1) * sizeof (char));
      rp = mempcpy (row, chunk, rlen);
      free (chunk);
      for (j=0; j<num_col; j++)
        {
          result = gsl_matrix_get (results, i, j);
          if (asprintf (&chunk, "\t%.10f", result) < 0)
            error (EXIT_FAILURE, errno, "Memory exhausted");
          chunklen = strlen (chunk);
          rp = extend_row (&row, &rlen, rp, chunk, chunklen);
          free (chunk);
        }
      *rp++ = '\0';
      puts (row);
      free (row);
    }
}


void
msa_joint_neff (alignment *msa)
{
  size_t i, j;
  double neff;
  gsl_matrix *joint_neff_cpy;
  /* 
   * msa->neff = gsl_vector_alloc (msa->length);
   * gsl_vector_set_zero (msa->neff);
   * for (i=0; i<msa->length; i++)
   *   {
   *     neff = calc_neff (*msa, i);
   *     gsl_vector_set (msa->neff, i, neff);
   *   }
   */
  msa->joint_neff = gsl_matrix_alloc (msa->length, msa->length);
  gsl_matrix_set_zero (msa->joint_neff);
#pragma omp parallel for default(shared) private(i,j,neff)
  for (i=0; i<msa->length; i++)
    {
      for (j=i+1; j<msa->length; j++)
        {
          neff = calc_joint_neff (*msa, i, j);
          gsl_matrix_set (msa->joint_neff, i, j, neff);
        }
    }
  joint_neff_cpy = gsl_matrix_alloc (msa->length, msa->length);
  gsl_matrix_transpose_memcpy (joint_neff_cpy, msa->joint_neff);
  gsl_matrix_add (msa->joint_neff, joint_neff_cpy);
}


void
precalc_joint_probs (alignment msa, copts opts, cparams *params)
{
  size_t i, j, pair_num, nseqs;
  size_t num_pairs = (msa.length * (msa.length - 1))/2;
  gsl_matrix *counts;
  params->joint_probs = (gsl_matrix **)xmalloc (num_pairs * sizeof (gsl_matrix *));
  for (i=0; i<(msa.length * (msa.length - 1))/2; i++)
    params->joint_probs[i] = NULL;
#pragma omp parallel for default(shared) private(i,j,pair_num,counts,nseqs)
  for (i=0; i<msa.length; i++)
    {
      for (j=i+1; j<msa.length; j++)
        {
          pair_num = pair_to_1d (msa.length, i, j);
          params->joint_probs[pair_num] = gsl_matrix_alloc (params->num_vars, params->num_vars);
          counts = gsl_matrix_alloc (params->num_vars, params->num_vars);
          nseqs = contingency_table (msa, i, j, counts, opts);
          /* Calculate joint probabilities. */
          calc_joint_probs (counts, msa, nseqs, i, j,
                            params->joint_probs[pair_num], opts, *params);
          gsl_matrix_free (counts);
        }
    }
}


int
compare_pos (const void *a, const void *b)
{
  const int *ia = (const int *) a;
  const int *ib = (const int *) b;

  return (*ia > *ib) - (*ia < *ib);
}


int
main (int argc, char **argv)
{
  struct arguments arguments;
  alignment msa, degapped_msa;
  size_t i, num_sites;
  copts opts = {false, false, false, false, false, false, false, false, false, -1.0};
  cparams params = {0.0, 0.0, 0.0, NULL, 0, '\0', '\0', NULL};
  gsl_matrix *results = NULL;

  init_args (&arguments);
  argp_parse (&argp, argc, argv, 0, 0, &arguments);

  /* Option errors */
  check_args (arguments);
  set_options (arguments, &opts, &params);

  /* Read in the alignment */
  read_alignment (arguments.args[0], &msa);

  remove_gappy_columns (msa, &degapped_msa, opts);

  if (opts.weight)
    calc_seq_weights (&degapped_msa);
  if (opts.add_pc && !opts.sp)
    msa_joint_neff (&degapped_msa);

  if (arguments.profile)
    {
      params.prf = read_profile (arguments.profile, degapped_msa.orig_cols,
                                 degapped_msa.length);
      if ((size_t)params.prf->p->size1 != degapped_msa.length)
        {
          fail_msg = "Alignment length and profile length do not match";
          goto exit;
        }
    }

  if (arguments.precalc_jp)
    {
      precalc_joint_probs (degapped_msa, opts, &params);
    }

  num_sites = degapped_msa.length;

  main_loop (degapped_msa, arguments, num_sites, opts, params, &results);

  if (!fail_msg)
    print_results (arguments, results, num_sites, degapped_msa, opts);

 exit:
  /* Clean up */
  destroy_alignment (&msa);
  destroy_alignment (&degapped_msa);
  if (arguments.profile && params.prf)
    destroy_profile (params.prf);
  if (arguments.precalc_jp && params.joint_probs)
    {
      for (i=0; i<(msa.length * (msa.length - 1))/2; i++)
        {
          if (params.joint_probs[i])
            gsl_matrix_free (params.joint_probs[i]);
        }
      free (params.joint_probs);
    }
  if (results)
    gsl_matrix_free (results);
  if (fail_msg)
    error (EXIT_FAILURE, errno, fail_msg);
  exit (EXIT_SUCCESS);
}

