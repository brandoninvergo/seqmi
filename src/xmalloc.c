/* 
 * xmalloc.c --- safe memory management
 * 
 * Copyright (C) 2014, 2015 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "xmalloc.h"

void *
xmalloc (size_t size)
{
  void *value = malloc (size);
  if (value == NULL)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  return value;
}

void *
xcalloc (size_t count, size_t size)
{
  void *value = calloc (count, size);
  if (value == NULL)
    error (EXIT_FAILURE, errno, "Memory exhausted");
  return value;
}

void *
xrealloc (void *ptr, size_t size)
{
  void *value = realloc (ptr, size);
  if (value == NULL)
    {
      free (ptr);
      error (EXIT_FAILURE, errno, "Memory exhausted");
    }
  return value;
}
