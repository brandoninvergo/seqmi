/* 
 * testprobs.c --- 
 * 
 * Copyright (C) 2014 Brandon Invergo <brandon@invergo.net>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <dejagnu.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include "probs.h"
#include "alignment.h"
#include "options.h"
#include "aminoacid.h"
#include "profile.h"
#include "data/test.h"

#define TOLERANCE 1e-7


short int
matrix_equal (gsl_matrix *m1, gsl_matrix *m2, size_t n)
{
  size_t i, j;
  double expect, got;
  for (i=0; i<n; i++)
    {
      for (j=0; j<n; j++)
        {
          expect = gsl_matrix_get (m1, i, j);
          got = gsl_matrix_get (m2, i, j);
          if (abs (expect - got) > TOLERANCE)
            return 0;
        }
    }
  return 1;
}


short int
vector_equal (gsl_vector *v1, gsl_vector *v2, size_t n)
{
  size_t i;
  double expect, got;
  for (i=0; i<n; i++)
    {
      expect = gsl_vector_get (v1, i);
      got = gsl_vector_get (v2, i);
      if (abs (expect - got) > TOLERANCE)
        return 0;
    }
  return 1;
}


void
print_matrix_diff (gsl_matrix *m1, gsl_matrix *m2, size_t n)
{
  size_t i, j;
  double expect, got;
  for (i=0; i<n; i++)
    {
      for (j=0; j<n; j++)
        {
          expect = gsl_matrix_get (m1, i, j);
          got = gsl_matrix_get (m2, i, j);
          if (abs (expect - got) > TOLERANCE)
            printf ("(%d, %d): expected %f, got %f\n", i, j, expect, got);
        }
    }
}


void
print_vector_diff (gsl_vector *v1, gsl_vector *v2, size_t n)
{
  size_t i;
  double expect, got;
  for (i=0; i<n; i++)
    {
      expect = gsl_vector_get (v1, i);
      got = gsl_vector_get (v2, i);
      if (abs (expect - got) > TOLERANCE)
        printf ("(%d): expected %f, got %f\n", i, expect, got);
    }
}


void
test_contingency_table (alignment msa)
{
  gsl_matrix *pos3_pos5_counts, *test_counts;
  size_t i, j, bad_cells;
  copts opts;
  opts.weight = 0;
  pos3_pos5_counts = gsl_matrix_alloc (NUM_AA, NUM_AA);
  test_counts = gsl_matrix_alloc (NUM_AA, NUM_AA);
  gsl_matrix_set_zero (pos3_pos5_counts);
  gsl_matrix_set (pos3_pos5_counts, MET, SER, 1.0);
  gsl_matrix_set (pos3_pos5_counts, MET, VAL, 1.0);
  gsl_matrix_set (pos3_pos5_counts, THR, ILE, 3.0);
  contingency_table (msa, 3, 5, test_counts, opts);
  if (gsl_matrix_equal (test_counts, pos3_pos5_counts))
    pass ("testprobs - Build a 2D contingency table");
  else
    {
      for (i=0; i<NUM_AA; i++)
        for (j=0; j<NUM_AA; j++)
          if (gsl_matrix_get (test_counts, i, j) !=
              gsl_matrix_get (pos3_pos5_counts, i, j))
            bad_cells++;
      fail ("testprobs - Build a 2D contingency table: %d bad cells",
            bad_cells);
      print_matrix_diff (pos3_pos5_counts, test_counts, NUM_AA);
    }
  gsl_matrix_free (pos3_pos5_counts);
  gsl_matrix_free (test_counts);
}


void
test_contingency_table_gaps (alignment msa)
{
  gsl_matrix *pos0_pos3_counts, *test_counts;
  size_t i, j, bad_cells, nseqs;
  copts opts;
  opts.weight = 0;
  pos0_pos3_counts = gsl_matrix_alloc (NUM_AA, NUM_AA);
  test_counts = gsl_matrix_alloc (NUM_AA, NUM_AA);
  gsl_matrix_set_zero (pos0_pos3_counts);
  gsl_matrix_set (pos0_pos3_counts, MET, THR, 3.0);
  nseqs = contingency_table (msa, 0, 3, test_counts, opts);
  if (gsl_matrix_equal (test_counts, pos0_pos3_counts))
    pass ("testprobs - Build a 2D contingency table with alignment gaps");
  else
    {
      for (i=0; i<NUM_AA; i++)
        for (j=0; j<NUM_AA; j++)
          if (gsl_matrix_get (test_counts, i, j) !=
              gsl_matrix_get (pos0_pos3_counts, i, j))
            bad_cells++;
      fail ("testprobs - Build a 2D contingency table with alignment gaps: %d "
            "bad cells", bad_cells);
      print_matrix_diff (pos0_pos3_counts, test_counts, NUM_AA);
    }
  if (nseqs == 3)
    pass ("testprobs - Don't count sequences with gaps");
  else
    fail ("testprobs - Don't count sequences with gaps");
  gsl_matrix_free (pos0_pos3_counts);
  gsl_matrix_free (test_counts);
}


void
test_binary_contingency_table (alignment msa)
{
  cparams params;
  gsl_matrix *pos3_pos5_counts, *test_counts;
  size_t i, j, bad_cells = 0;
  copts opts;
  opts.weight = 0;
  params.res1 = 'M';
  params.res2 = 'S';
  pos3_pos5_counts = gsl_matrix_alloc (2, 2);
  test_counts = gsl_matrix_alloc (2, 2);
  gsl_matrix_set_zero (pos3_pos5_counts);
  gsl_matrix_set (pos3_pos5_counts, 0, 0, 1.0);
  gsl_matrix_set (pos3_pos5_counts, 0, 1, 1.0);
  gsl_matrix_set (pos3_pos5_counts, 1, 0, 0.0);
  gsl_matrix_set (pos3_pos5_counts, 1, 1, 3.0);
  binary_contingency_table (msa, 3, 5, test_counts, opts, params);
  if (gsl_matrix_equal (test_counts, pos3_pos5_counts))
    pass ("testprobs - Build a 2D binary contingency table");
  else
    {
      for (i=0; i<2; i++)
        for (j=0; j<2; j++)
          if (gsl_matrix_get (test_counts, i, j) !=
              gsl_matrix_get (pos3_pos5_counts, i, j))
            bad_cells++;
      fail ("testprobs - Build a 2D binary contingency table: %d bad cells",
            bad_cells);
      print_matrix_diff (pos3_pos5_counts, test_counts, 2);
    }
  gsl_matrix_free (pos3_pos5_counts);
  gsl_matrix_free (test_counts);
}


void
test_binary_contingency_table_gaps (alignment msa)
{
  cparams params;
  gsl_matrix *pos0_pos3_counts, *test_counts;
  size_t i, j, bad_cells = 0, nseqs;
  copts opts;
  opts.weight = 0;
  params.res1 = 'M';
  params.res2 = 'T';
  pos0_pos3_counts = gsl_matrix_alloc (2, 2);
  test_counts = gsl_matrix_alloc (2, 2);
  gsl_matrix_set_zero (pos0_pos3_counts);
  gsl_matrix_set (pos0_pos3_counts, 0, 0, 3.0);
  nseqs = binary_contingency_table (msa, 0, 3, test_counts, opts, params);
  if (gsl_matrix_equal (test_counts, pos0_pos3_counts))
    pass ("testprobs - Build a 2D binary contingency table with alignment gaps");
  else
    {
      for (i=0; i<2; i++)
        for (j=0; j<2; j++)
          if (gsl_matrix_get (test_counts, i, j) !=
              gsl_matrix_get (pos0_pos3_counts, i, j))
            bad_cells++;
      fail ("testprobs - Build a 2D binary contingency table with alignment "
            "gaps: %d bad cells", bad_cells);
      print_matrix_diff (pos0_pos3_counts, test_counts, 2);
    }
  if (nseqs == 3)
    pass ("testprobs - Don't count sequences with gaps (binary table)");
  else
    fail ("testprobs - Don't count sequences with gaps (binary table): %d "
          "counted", nseqs);
  gsl_matrix_free (pos0_pos3_counts);
  gsl_matrix_free (test_counts);
}


void
test_calc_joint_probs_ml (alignment msa)
{
  gsl_matrix *pos3_pos5_jp, *test_jp;
  gsl_matrix *counts;
  size_t nseqs, i, j, bad_cells = 0;
  cparams params;
  copts opts;
  opts.sp = 0;
  opts.pp = 0;
  opts.cj = 0;
  opts.weight = 0;
  params.num_vars = NUM_AA;
  counts = gsl_matrix_alloc (NUM_AA, NUM_AA);
  pos3_pos5_jp = gsl_matrix_alloc (NUM_AA, NUM_AA);
  test_jp = gsl_matrix_alloc (NUM_AA, NUM_AA);
  gsl_matrix_set_zero (pos3_pos5_jp);
  gsl_matrix_set (pos3_pos5_jp, char2aa ('M'), char2aa ('S'), 0.2);
  gsl_matrix_set (pos3_pos5_jp, char2aa ('M'), char2aa ('V'), 0.2);
  gsl_matrix_set (pos3_pos5_jp, char2aa ('T'), char2aa ('I'), 0.6);
  nseqs = contingency_table (msa, 3, 5, counts, opts);
  calc_joint_probs (counts, msa, nseqs, 3, 5, test_jp, opts, params);
  if (matrix_equal (pos3_pos5_jp, test_jp, NUM_AA))
    pass ("testprobs - Calculate joint probabilities");
  else
    {
      for (i=0; i<NUM_AA; i++)
        for (j=0; j<NUM_AA; j++)
          if (gsl_matrix_get (test_jp, i, j) !=
              gsl_matrix_get (pos3_pos5_jp, i, j))
            bad_cells++;
      fail ("testprobs - Calculate joint probabilities: %d bad cells",
            bad_cells);
      print_matrix_diff (pos3_pos5_jp, test_jp, NUM_AA);
    }
  gsl_matrix_free (pos3_pos5_jp);
  gsl_matrix_free (test_jp);
  gsl_matrix_free (counts);
}


void
test_calc_joint_probs_sp (alignment msa)
{
  gsl_matrix *pos3_pos5_jp, *test_jp;
  gsl_matrix *counts;
  size_t nseqs, i, j, bad_cells = 0;
  cparams params;
  copts opts;
  double lambda = 0.05;
  double denom = 5.0 + (double) NUM_AA * lambda;
  opts.sp = 1;
  opts.pp = 0;
  opts.cj = 0;
  opts.weight = 0;
  params.num_vars = NUM_AA;
  params.sp_lambda = lambda;
  counts = gsl_matrix_alloc (NUM_AA, NUM_AA);
  pos3_pos5_jp = gsl_matrix_alloc (NUM_AA, NUM_AA);
  test_jp = gsl_matrix_alloc (NUM_AA, NUM_AA);
  gsl_matrix_set_all (pos3_pos5_jp, lambda/denom);
  gsl_matrix_set (pos3_pos5_jp, char2aa ('M'), char2aa ('S'),
                  (1.0+lambda)/denom);
  gsl_matrix_set (pos3_pos5_jp, char2aa ('M'), char2aa ('V'),
                  (1.0+lambda)/denom);
  gsl_matrix_set (pos3_pos5_jp, char2aa ('T'), char2aa ('I'),
                  (3.0+lambda)/denom);
  nseqs = contingency_table (msa, 3, 5, counts, opts);
  calc_joint_probs (counts, msa, nseqs, 3, 5, test_jp, opts, params);
  if (matrix_equal (pos3_pos5_jp, test_jp, NUM_AA))
    pass ("testprobs - Calculate joint probabilities (simple pseudocount)");
  else
    {
      for (i=0; i<NUM_AA; i++)
        for (j=0; j<NUM_AA; j++)
          if (gsl_matrix_get (test_jp, i, j) !=
              gsl_matrix_get (pos3_pos5_jp, i, j))
            bad_cells++;
      fail ("testprobs - Calculate joint probabilities (simple pseudocount): %d "
            "bad cells", bad_cells);
      print_matrix_diff (pos3_pos5_jp, test_jp, NUM_AA);
    }
  gsl_matrix_free (pos3_pos5_jp);
  gsl_matrix_free (test_jp);
  gsl_matrix_free (counts);  
}


void
test_calc_joint_probs_pp (alignment msa)
{
  gsl_matrix *test_jp;
  gsl_matrix_view prf_cross, pos3_pos5_jp;
  gsl_matrix *counts;
  size_t nseqs, i, j, bad_cells = 0;
  cparams params;
  copts opts;
  double a = 0.5;
  double b = 1.0;
  double tau = (1.0 + b)/(exp (a * POS3_POS5_NEFF) + b);
  opts.sp = 0;
  opts.pp = 1;
  opts.cj = 0;
  opts.weight = 0;
  opts.marg_prf = 1;
  params.num_vars = NUM_AA;
  params.pp_a = a;
  params.pp_b = b;
  params.prf = read_profile ("data/test.prf");
  counts = gsl_matrix_alloc (NUM_AA, NUM_AA);
  pos3_pos5_jp = gsl_matrix_view_array (POS3_POS5_JP_PP, NUM_AA, NUM_AA);
  test_jp = gsl_matrix_alloc (NUM_AA, NUM_AA);
  nseqs = contingency_table (msa, 3, 5, counts, opts);
  calc_joint_probs (counts, msa, nseqs, 3, 5, test_jp, opts, params);
  if (matrix_equal (&pos3_pos5_jp.matrix, test_jp, NUM_AA))
    pass ("testprobs - Calculate joint probabilities (profile-based "
          "pseudocount)");
  else
    {
      for (i=0; i<NUM_AA; i++)
        for (j=0; j<NUM_AA; j++)
          if (gsl_matrix_get (test_jp, i, j) !=
              gsl_matrix_get (&pos3_pos5_jp.matrix, i, j))
            bad_cells++;
      fail ("testprobs - Calculate joint probabilities (profile-based "
            "pseudocount): %d bad cells", bad_cells);
      print_matrix_diff (&pos3_pos5_jp.matrix, test_jp, NUM_AA);
    }
  gsl_matrix_free (test_jp);
  gsl_matrix_free (counts);  
}


void
test_calc_joint_probs_cj (alignment msa)
{
  /* TODO */
  xfail ("testprobs - Calculate joint probabilities (profile-constrained): test "
         "not implemented");
}


void
test_calc_marg_probs_ml (alignment msa)
{
  gsl_vector *pos3_mp, *pos5_mp, *test3_mp, *test5_mp;
  gsl_matrix *joint_probs;
  size_t i, bad_cells;
  cparams params;
  copts opts;
  opts.marg_prf = 0;
  params.num_vars = NUM_AA;
  joint_probs = gsl_matrix_alloc (NUM_AA, NUM_AA);
  pos3_mp = gsl_vector_alloc (NUM_AA);
  pos5_mp = gsl_vector_alloc (NUM_AA);
  test3_mp = gsl_vector_alloc (NUM_AA);
  test5_mp = gsl_vector_alloc (NUM_AA);
  gsl_matrix_set_zero (joint_probs);
  gsl_matrix_set (joint_probs, char2aa ('M'), char2aa ('S'), 0.2);
  gsl_matrix_set (joint_probs, char2aa ('M'), char2aa ('V'), 0.2);
  gsl_matrix_set (joint_probs, char2aa ('T'), char2aa ('I'), 0.6);
  gsl_vector_set_zero (pos3_mp);
  gsl_vector_set_zero (pos5_mp);
  gsl_vector_set (pos3_mp, char2aa ('M'), 0.4);
  gsl_vector_set (pos3_mp, char2aa ('T'), 0.6);
  gsl_vector_set (pos5_mp, char2aa ('S'), 0.2);
  gsl_vector_set (pos5_mp, char2aa ('V'), 0.2);
  gsl_vector_set (pos5_mp, char2aa ('I'), 0.6);
  calc_marg_probs (joint_probs, test3_mp, test5_mp, 3, 5, opts, params);
  if (vector_equal (pos3_mp, test3_mp, NUM_AA))
      pass ("testprobs - Calculate marginal probabilities: position 4");
  else
    {
      bad_cells = 0;
      if (!vector_equal (pos3_mp, test3_mp, NUM_AA))
        {
          for (i=0; i<NUM_AA; i++)
            if (gsl_vector_get (test3_mp, i) != gsl_vector_get (pos3_mp, i))
              bad_cells++;
          fail ("testprobs - Calculate marginal probabilities: %d bad cells for "
                "position 4", bad_cells);
          print_vector_diff (pos3_mp, test3_mp, NUM_AA);
        }
    }      
  if (vector_equal (pos5_mp, test5_mp, NUM_AA))
      pass ("testprobs - Calculate marginal probabilities: position 6");
  else
    {
      bad_cells = 0;
      if (!vector_equal (pos5_mp, test5_mp, NUM_AA))
        {
          for (i=0; i<NUM_AA; i++)
            if (gsl_vector_get (test5_mp, i) != gsl_vector_get (pos5_mp, i))
              bad_cells++;
          fail ("testprobs - Calculate marginal probabilities: %d bad cells for "
                "position 6", bad_cells);
          print_vector_diff (pos5_mp, test5_mp, NUM_AA);
        }
    }
  gsl_matrix_free (joint_probs);
  gsl_vector_free (pos3_mp);
  gsl_vector_free (pos5_mp);
  gsl_vector_free (test3_mp);
  gsl_vector_free (test5_mp);
}


void
test_calc_marg_probs_prf (alignment msa)
{
  gsl_vector *pos3_mp, *pos5_mp, *test3_mp, *test5_mp;
  gsl_matrix *joint_probs;
  size_t i, bad_cells;
  cparams params;
  copts opts;
  opts.marg_prf = 1;
  params.num_vars = NUM_AA;
  params.prf = read_profile ("data/test.prf");
  joint_probs = gsl_matrix_alloc (NUM_AA, NUM_AA);
  pos3_mp = gsl_vector_alloc (NUM_AA);
  pos5_mp = gsl_vector_alloc (NUM_AA);
  test3_mp = gsl_vector_alloc (NUM_AA);
  test5_mp = gsl_vector_alloc (NUM_AA);
  gsl_vector_set_zero (pos3_mp);
  gsl_vector_set_zero (pos5_mp);
  gsl_vector_set (pos3_mp, char2aa ('M'), 0.0171934);  /* 5862 */
  gsl_vector_set (pos3_mp, char2aa ('T'), 0.0495834);  /* 4334 */
  gsl_vector_set (pos5_mp, char2aa ('S'), 0.0373694);  /* 4742 */
  gsl_vector_set (pos5_mp, char2aa ('V'), 0.0380753);  /* 4715 */
  gsl_vector_set (pos5_mp, char2aa ('I'), 0.0246204);  /* 5344 */
  calc_marg_probs (joint_probs, test3_mp, test5_mp, 3, 5, opts, params);
  if (vector_equal (pos3_mp, test3_mp, NUM_AA))
      pass ("testprobs - Calculate marginal probabilities (profile): position "
            "4");
  else
    {
      bad_cells = 0;
      if (!vector_equal (pos3_mp, test3_mp, NUM_AA))
        {
          for (i=0; i<NUM_AA; i++)
            if (gsl_vector_get (test3_mp, i) != gsl_vector_get (pos3_mp, i))
              bad_cells++;
          fail ("testprobs - Calculate marginal probabilities (profile): %d bad "
                "cells for position 4", bad_cells);
          print_vector_diff (pos3_mp, test3_mp, NUM_AA);
        }
    }      
  if (vector_equal (pos5_mp, test5_mp, NUM_AA))
      pass ("testprobs - Calculate marginal probabilities (profile): position "
            "6");
  else
    {
      bad_cells = 0;
      if (!vector_equal (pos5_mp, test5_mp, NUM_AA))
        {
          for (i=0; i<NUM_AA; i++)
            if (gsl_vector_get (test5_mp, i) != gsl_vector_get (pos5_mp, i))
              bad_cells++;
          fail ("testprobs - Calculate marginal probabilities (profile): %d bad "
                "cells for position 6", bad_cells);
          print_vector_diff (pos5_mp, test5_mp, NUM_AA);
        }
    }
  gsl_matrix_free (joint_probs);
  gsl_vector_free (pos3_mp);
  gsl_vector_free (pos5_mp);
  gsl_vector_free (test3_mp);
  gsl_vector_free (test5_mp);
}


int
main (void)
{
  alignment msa;
  read_alignment ("data/test.fasta", &msa);
  test_contingency_table (msa);
  test_contingency_table_gaps (msa);
  test_binary_contingency_table (msa);
  test_binary_contingency_table_gaps (msa);
  test_calc_joint_probs_ml (msa);
  test_calc_joint_probs_sp (msa);
  test_calc_joint_probs_pp (msa);
  test_calc_joint_probs_cj (msa);
  test_calc_marg_probs_ml (msa);
  test_calc_marg_probs_prf (msa);
  destroy_alignment (&msa);
  return EXIT_SUCCESS;
}
